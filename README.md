# A Prototype for a Digital Edition of Antonio de León Pinelo’s _Epítome de la biblioteca oriental y occidental, náutica y geográfica_ (1629)
## Edited by Clayton McCarl, University of North Florida
## Digital Edition code by Raffaele Viglianti and Clayton McCarl

### Scholarly Editing, Volume 39, 2022

This micro-edition is published on the [Scholarly Editing website](https://scholarlyediting.org/issues/39/a-prototype-for-a-digital-edition-of-antonio-de-leon-pinelos-epitome-de-la-biblioteca-oriental-y-occidental-nautica-y-geografica-1629).

The repository is **archived**. No further changes will be made to this repository. For a general micro-edition template please see [this GitLab repository](https://gitlab.com/scholarly-editing/se-microedition-template).
