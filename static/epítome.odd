<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
  <teiHeader>
    <fileDesc>
      <titleStmt>
        <title>TEI customization for A Prototype for a Digital Edition of Antonio de León Pinelo&#x27;s
          <title level="m">Epítome de la biblioteca oriental y occidental, náutica y
            geográfica</title> (1629) </title>
        <author>Clayton McCarl</author>
        <author>Raff Viglianti</author>
      </titleStmt>
      <publicationStmt>
        <publisher>Scholaryl Editing Journal</publisher>
        <availability status="free">
          <licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/"> Distributed under
            aCreative Commons Attribution-NonCommercial-ShareAlike 4.0 International
            License.</licence>
        </availability>
      </publicationStmt>
      <sourceDesc>
        <p>Based on TEI minimal by James Cummings and partly compiled via
          https://romabeta.tei-c.org</p>
      </sourceDesc>
    </fileDesc>
  </teiHeader>
  <text>
    <body>
      <schemaSpec ident="tei_epitome" start="TEI" prefix="tei_" targetLang="en" docLang="en">

        <moduleRef key="namesdates"/>
        <moduleRef key="header"
          include="teiHeader fileDesc titleStmt publicationStmt sourceDesc authority idno availability editionStmt edition seriesStmt extent"/>

        <moduleRef key="core"
          include="p title bibl listBibl author textLang editor respStmt address publisher date resp name addrLine ref foreign pb head pubPlace citedRange note mentioned q sic"/>

        <moduleRef key="textstructure" include="TEI text body"/>

        <moduleRef key="tei"/>


        <moduleRef key="linking" include="seg ab"/>
        <moduleRef key="msdescription" include="dim"/>

        <elementSpec ident="seg" mode="change">
          <desc>In this customization <gi>seg</gi> can be used to provide bilingual vesrions of
              <gi>bibl</gi> elements, so the content model of <gi>seg</gi> is extended to match the
            one of <gi>bibl</gi>.</desc>
          <content>
            <alternate maxOccurs="unbounded" minOccurs="0">
              <textNode/>
              <classRef key="model.gLike"/>
              <classRef key="model.highlighted"/>
              <classRef key="model.pPart.data"/>
              <classRef key="model.pPart.edit"/>
              <classRef key="model.segLike"/>
              <classRef key="model.ptrLike"/>
              <classRef key="model.biblPart"/>
              <classRef key="model.global"/>
            </alternate>
          </content>
        </elementSpec>
        <classRef key="att.global.facs"/>

        <elementSpec ident="listBibl" mode="change">
          <desc>In this customization, listBibl can contain <gi>person</gi>. I have taken a special
            approach to the entries in Título XXVI ("Colectores de libros de Indias") of the
            Biblioteca Occidental, which list people, not books. My content model of lists and books
            includes people as subordinate elements within <gi>bibl</gi> (as <gi>author</gi>,
              <gi>editor</gi>, etc.), not as a type of object in parallel with <gi>bibl</gi> itself.
            However, the título in question breaks that paradigm. As a provisional solution, I have
            marked these entries with <gi>person</gi>, which under the P5 standard is allowed within
              <gi>listBibl</gi>&gt; and which can contain <gi>bibl</gi>. In doing so, I have
            employed <gi>person</gi>, in a sense, as a layer between <gi>listBibl</gi> and
              <gi>bibl</gi>
          </desc>
          <content>
            <sequence>
              <classRef key="model.headLike" minOccurs="0" maxOccurs="unbounded"/>
              <elementRef key="desc" minOccurs="0" maxOccurs="unbounded"/>
              <alternate minOccurs="0" maxOccurs="unbounded">
                <classRef key="model.milestoneLike" minOccurs="1" maxOccurs="1"/>
                <elementRef key="relation" minOccurs="1" maxOccurs="1"/>
                <elementRef key="listRelation" minOccurs="1" maxOccurs="1"/>
              </alternate>
              <sequence minOccurs="1" maxOccurs="unbounded">
                <classRef key="model.biblLike" minOccurs="1" maxOccurs="unbounded"/>
                <alternate minOccurs="0" maxOccurs="unbounded">
                  <classRef key="model.milestoneLike" minOccurs="1" maxOccurs="1"/>
                  <elementRef key="relation" minOccurs="1" maxOccurs="1"/>
                  <elementRef key="listRelation" minOccurs="1" maxOccurs="1"/>
                  <elementRef key="person" minOccurs="1" maxOccurs="unbounded"/>
                </alternate>
              </sequence>
            </sequence>
          </content>
        </elementSpec>
        
        <elementSpec ident="person" mode="change">
          <classes mode="change">
            <memberOf key="att.typed"/>
          </classes>
        </elementSpec>
      </schemaSpec>
    </body>
  </text>
</TEI>
