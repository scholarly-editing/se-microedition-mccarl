const basePath = process.env.BASEPATH
const title = "A Prototype for a Digital Edition of Antonio de León Pinelo’s Epítome de la biblioteca oriental y occidental, náutica y geográfica (1629)"

module.exports = {
  pathPrefix: basePath,
  siteMetadata: {
    issue: "Volume 39",
    doi: '10.55520/12N3RNQ6',
    group_order: 2,
    title: title,
    htmlTitle: "A Prototype for a Digital Edition of Antonio de León Pinelo’s <em>Epítome de la biblioteca oriental y occidental, náutica y geográfica</em> (1629)",
    description: `A Scholarly Editing micro-edition. ${title}. Edited by Clayton McCarl.`,
    authors: [
      {
        "first": "Clayton",
        "middle": "",
        "last": "McCarl",
        "affiliations": [
          "University of North Florida"
        ],
        "orcid": ""
      }
    ],
    menuLinks: [
      {
        name: 'introducción',
        link: '/'
      },
      {
        name: 'introduction',
        link: '/introduction'
      },
      {
        name: 'edition',
        link: '/epítome'
      },
    ]
  },
  plugins: [
    `gatsby-plugin-material-ui`,
    `gatsby-plugin-image`,
    `gatsby-plugin-sharp`,
    `gatsby-transformer-sharp`,
    `gatsby-theme-ceteicean`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        path: `static/tei`,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        path: `${__dirname}/src/images/`,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `introduction`,
        path: `src/introduction`,
      },
    },
    `gatsby-transformer-remark`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `Scholarly Editing`,
        short_name: `Scholarly Editing`,
        start_url: `/`,
        icon: `src/images/se-icon.png`, // This path is relative to the root of the site.
      },
    },
  ],
}
