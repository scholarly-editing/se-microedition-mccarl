---
path: "/introduction"
lang: "en"
title: "A Prototype for a Digital Edition of Antonio de León Pinelo’s <em>Epítome de la biblioteca oriental y occidental, náutica y geográfica</em> (1629)"
---

Antonio de León Pinelo's _Epítome de la biblioteca oriental y occidental, náutica y geográfica_ (Summary of the Library of the East and West Indies and the Nautical and Geographical Arts, 1629) is considered the foundational text of Americanist bibliography, and represents an essential source for scholars of Hispanic letters and the early modern maritime world.[^1] It is also a singular object that combines an inchoate bibliographic practice with political and autobiographical discourse. Despite the importance of the _Epítome_, however, no modern edition exists.

This is a prototype for such a version, carried out in a digital format. This edition of the _Epítome_ is designed to facilitate interaction with the text in two modalities: as a rhetorical object and a repository of data. This is accomplished through the use of a custom vocabulary for semantic markup and the addition of formalized data.

The editorial strategy reflected here is explained in the article “Semantic Markup and Structured Annotation of Early Modern Bibliographies,” published in this same issue of _Scholarly Editing_. That essay outlines a conceptual model for understanding this type of object from an editorial perspective, drawing examples from León Pinelo’s work. It also includes reflections on the applicability of the model to other projects involving bibliographies and other types of texts.

Because the theoretical framework and the larger context of this project are established in that piece, this introduction addresses only matters specific to the creation of the prototype itself. I explain how I have implemented the conceptual paradigm outlined in the article, and discuss the interface developed to display the sample edition. I likewise review the editorial criteria and the approach to translation. 

This experiment is intended primarily as a tool for visualizing the concept model described in the article “Semantic Markup and Structured Annotation of Early Modern Bibliographies,” as well as the TEI-XML implementation of that paradigm explained in section 1.0 (“Encoding”) of this introduction. As such, it is limited in terms of scope and functionality. For instance, it addresses the only bibliographical section of the _Epítome_, setting aside for now the preliminary texts in prose and verse. Most notably, the interface only allows interaction with the text in its original linear format. 

A future stage of this project will examine ways that the data contained in the edition might be exploited in a nonlinear fashion. That phase will involve, among other matters, the creation of a system of authorities for such entities as people, places, and languages that can facilitate an interactive index and shared annotations. It will also involve the development of tools for visualizing the _Epítome_ chronologically and geographically. 

## 1.0 Encoding

This edition utilizes TEI-XML following the P5 Guidelines. Most of the elements that are central to this project are explained in subsection 3.11, “Bibliographic Citations and References,” within section 3, “Elements Available in All TEI Documents.”[^2] With the exception of two minor customizations, I have not modified the P5 schema.[^3] 

### 1.1 Marking Up the Text Itself

The conceptual model that underlies this project consists primarily of two types of entities: lists and books, with the latter category encompassing any type of bibliographic object. Following this paradigm, the building blocks of this implementation are lists, expressed as `<listBibl>`, and books, articulated as `<bibl>`. 

The model allows for the nesting of these objects within each other. This can involve the embedding of books within lists and lists within other lists. The paradigm also allows for the nesting of lists and books within books, based on the idea that the book functions as a type of conceptual container. 

In this sample implementation, the high-level object is a `<listBibl>`, corresponding to the bibliographic section of the _Epítome_. That `<listBibl>`, in turn, contains four others, one for each of the main divisions, or _bibliotecas_, of León Pinelo’s book. Within each of those four larger sections, every geographical or thematic subdivision — or _título_ (title) as the bibliographer labels them — is also a `<listBibl>`.

Inside those subdivisions, each of León Pinelo’s main entries (the first that appears for a given work) is encoded as a `<bibl>` with a value of “orig” for @type, and "primary” for @subtype. Nested inside that `<bibl>` are any additional items that León Pinelo enumerates for that entry, each as a `<bibl>` with a value of “orig” for @type, and a value for @subtype of “other_edition,” “translation,” or “misc” (for any related item that is something other than an edition or translation).

Within each `<bibl type="orig">`, the information is marked up with the elements that can comprise a bibliographic reference. These include `<author>`, `<editor>`, `<title>`, `<pubPlace>`, `<publisher>`, and `<date>`. I use `<textLang>` to encode León Pinelo’s references to the language in which a book is written, and `<dim>` for his specifications about the format of a print volume (_quarto_, _folio_, etc.). The markup of the `<bibl type="orig">` entries into the constituent fields follows the often unpredictable format of the text itself, and thus is necessarily irregular and reflects at times subjective decisions.

I employ the elements corresponding to bibliographic fields in a standard fashion, with three primary exceptions. For `<title>`, I use @type to indicate when the enclosed material is not an actual title but rather a description (type="desc"). This is necessary because León Pinelo’s entries often present the latter. At times the true nature of a title (whether it is a title or a description) is unclear, and in such cases, I opt for treating what León Pinelo offers as an actual title. I also represent as a title material that León Pinelo seems to regard as such, even when that is not actually the case. In other words, I consider a title to be a title — not a description — when it seems that the bibliographer has understood it that way, even when it may not be the correct or complete title of the work in question, or even when the existence of that work is in doubt. 

I have specified a value for @type of “pub” on all publication dates, to differentiate them from other contextual dates mentioned within a `<bibl>`. 

I have also marked up the state of a text as manuscript or print. Since P5 does not provide an element for doing so within `<bibl>`, I have encoded this as `<seg type="semantic" subtype="state">`.

I assign a unique identifier to every primary item in the _Epítome_ using @xml:id on the respective `<bibl>` element, basing this value on the author’s last name. I use canonical forms when possible (for instance, for _Linzcotan_ I use _xml:id="linschoten"_), but employ León Pinelo’s renderings in cases in which the author in question cannot be identified (for _fray Gaspar_, for example, I use _xml:id="gaspar"_, 58). When multiple authors have identical surnames, I add the first initial. When more than one work by a given author is included, year is appended, and when more than one are present for a given year, I add a title keyword (e.g., _xml:id="herreraMaldonado1620Historia"_ and _xml:id="herreraMaldonado1620Epitome"_). I add “Ed,” “Trans,” or “Misc,” followed by a year, to identify the other editions, translations, and miscellaneous items that correspond to a primary entry (_xml:id="linschotenTrans1599"_). When more than one translation were published in a given year, I include language (_xml:id="linschotenTrans1599Latin"_ and _xml:id="linschotenTrans1599German"_). This system of human-readable identifiers is designed to simplify the process of creating cross-references.

I have used @xml:lang to indicate the language of every component of the edition. All material corresponding to the original text is enclosed within `<seg>` elements marked xml:lang="es", as are the Spanish versions of all components of the critical apparatus. My translation of the text itself, along with all aspects of the apparatus in English, are contained in `<seg>` elements marked xml:lang="en". The Spanish and English `<seg>` elements are encoded together within the respective `<bibl>` element, or whatever other object contains them. 

I have taken a special approach to the entries in Título XXVI (“Colectores de libros de Indias”) of the Western Library, which lists people, not books. My content model of lists and books includes people as subordinate elements within `<bibl>` (as `<author>`, `<editor>`, etc.), not as a type of object in parallel with `<bibl>` itself. However, the _título_ in question breaks that paradigm. As a provisional solution, I have marked these entries with `<person>`, and in doing so have employed that element as a layer between `<listBibl>` and `<bibl>`.

### 1.2 Supplementary Markup

By _supplementary markup_, I refer to the encoding that serves not to organize, describe, or represent the text itself, but rather to provide mechanisms for analysis and interpretation. I discuss here two types: semantic markup and annotation. The first is divided into three categories: contextual, bibliographic, and discursive. The second encompasses two types: structured and unstructured.

By _contextual markup_, I refer to the encoding of information that relates to the historical circumstances discussed in the entries. I have marked up three types: names of people, names of places, and dates. I have encoded these as `<persName>`, `<placeName>`, and `<date>`, respectively. For `<placeName>` I stipulate values for @type following a fixed taxonomy that includes “settlement,” “nation,” “region,” and “topographical.” For `<date>` I use a value for @type of “contextual,” to differentiate from publication dates.[^4]  I mark up these contextual items only in the text of León Pinelo’s original entries (`<bibl type="orig">`). In the English version, when titles are translated in parentheses, I mark the contextual items in the translated title only, as the user can refer to the original Spanish for the corresponding markup.[^5]

In pointing to _bibliographical markup_ as a category, I refer to the encoding of the bibliographic fields that make up León Pinelo’s entries. I discussed this above as part of the process of describing the text, since on a basic level, isolating such entities as `<author>`, `<title>`, and `<pubPlace>` is an aspect of indicating what is already present in the text. These items are structural units, existing as the component parts that comprise the whole of a bibliographical reference, just as pages, chapters, paragraphs, and so on, comprise the structure of a book.

In addition to having such a descriptive function, _bibliographical markup_ also represents a category of semantic markup because this activity is related to the meaning of the text. The labeling such items as `<author>`, `<title>`, and `<pubPlace>` is in part an interpretive act. This is apparent in the subjective decisions that must be made, particularly with respect to `<title>`. Such elements are therefore reflective not only of the structure but also the meaning of the text.

By _discursive markup_, I refer to the seven rhetorical categories that I establish in “Semantic Markup and Structured Annotation of Early Modern Bibliographies.” To delineate the corresponding sections of text, I use `<seg>` with a value of “semantic” for @type and a value for @subtype that describes the discursive function in question. As explained in the article, a passage in which León Pinelo expresses concern over the accuracy of his information, or that of others, is marked with “doubt.” A value of “criticism” is used for text in which the bibliographer appears to critique the work or ideas of other authors, or articulate judgement about society more broadly. When León Pinelo rectifies errors present in other books, the passages in question are designated as “correction.” Sections in which he celebrates other authors and influential individuals are marked as “praise,” and “self-promotion” is used for occassions with which he seeks to advance his own reputation or that of his work. A value of “pub_status” labels places in which León Pinelo calls attention to the unpublished condition of certain texts, often stating or implying that action should be taken to remedy the situation.[^6] Passages are designated as “witness” when the bibliographer testifies to certain facts, usually because he has personally seen an unpublished manuscript or a hard-to-find book in a particular collection.

The structured annotation of the text is accomplished by extending the paradigm of lists (expressed as `<listBibl>`) and bibliographic objects (expressed as `<bibl>`). This is done by nesting within a given `<bibl type="orig">` additional bibliographic items that serve to explain or contextualize the enclosing entry. Each exists as a `<bibl>` with a value of “add” for @type, and a value for @subtype that reflects the nature or function of the inserted object.

A `<bibl type="add">` element with a value for @subtype of “author_source” indicates the book from which León Pinelo gathered his information, when this is stated or can be ascertained. The full bibliographic information for that volume is included unless the work in question has been cited previously, in which case a `<ref>` element is used, enclosing just the last name of the author (or author, year, and short title, as needed) with a value for @target that points to the @xml:id of the `<bibl>` referenced. I use `<citedRange>` to indicate the place within that source from which León Pinelo has drawn his information, when that is indicated or can be determined. I employ `<q>` to provide, when possible, a quotation that shows the language that León Pinelo cites or from which he otherwise likely extracted his information. When I know or can speculate with reasonable certainty on the source, in turn, of León Pinelo’s source, I nest an additional “author source” `<bibl>` containing a similar set of information. See, for example, the volume by “Pedro Aliosio”, for which León Pinelo cites Michaele Routartio, who appears to have drawn from the work of Antonio Posevino.[^7] 

A `<bibl type="add">` element with a value of “regularized” for @subtype presents the full bibliographic information for the item that León Pinelo mentions, drawn either from a copy of the object itself or from some secondary source. In the latter case, another `<bibl type="add">` with a value of “editor_source” for @type is nested within the respective `<bibl type="add" subtype="regularized">` element, with `<citedRange>` pointing to the location in that text, as appropriate,[^8] and `<q>` containing a quotation of the source material, when possible. 

While `<bibl type="add">` elements with subtypes of “author_source,” “regularized,” and “editor_source” are directed at explaining and clarifying León Pinelo’s references, two additional types of `<bibl type="add">` serve to facilitate the user’s access to the texts in question. The first, with a subtype of “digital_version,” points to an online edition or facsimile. When the bibliographic data for that online version are identical to the information in the respective `<bibl type="add" subtype="regularized">`, I include only the name of the container (_Google Books_, etc.), using `<ref>` to link to the corresponding URL (or DOI, etc.). If different, I include the complete bibliographic information. The second, with a subtype of “modern_edition,” can contain the information for a recent edition of the text. A given item could contain multiple instances of `<bibl>` with subtypes of “other_edition” and “digital_version.”

When the object contained in any `<bibl type="add">` was published without the explicit name of the author, I include a standardized version of the author’s name in square brackets when that name is known. This is often the case, for instance, with works gathered in collections such as those of Giovanni Battista Ramusio and Theodor de Bry. When listing those collections in their entirety, I use the name of the editor as author, in square brackets if not explicitly indicated on the title pages of the volume or volumes in question, followed by “ed.” in square brackets. I have handled in this fashion, for instance, the entries for the _Novus orbis_ of Caspar Barlaeus.

When the full bibliographic information for an item in a `<bibl type="add">` has already been stated elsewhere, I do not enter the information redundantly, but rather provide a cross-reference. I link to the regularized form (`<bibl type="add" subtype="regularized">`), unless the reference is to León Pinelo’s entry itself, in which case I link to the corresponding `<bibl type="orig">`. 

The naming scheme for xml:id on `<bibl type="add">` elements with subtypes of “editor_source,” “author_source,” “other_edition,” and “digital_version” follows the pattern for the `<bibl type="orig">` entries, as described above, since these entries are starting points, like the `<bibl type="orig">` items, not the restating of existing entries, as in the case of `<bibl type="add" subtype="regularized">`. When assigning a value for @xml:id on those `<bibl type="add" subtype="regularized">` items, I differentiate them from the original entries to which they correspond by appending “Reg.” For instance, León Pinelo’s entry for Van Linschoten has a value for @xml:id of “linschoten,” and the value for @xml:id of the corresponding regularized entry is “linschotenReg”. Likewise, his entry for the 1599 translation has a value for @xml:id of “linschotenTrans1599,” and the value for the corresponding regularized version is “linschotenTrans1599Reg.” 

Traditional prose notes — to which I refer as “unstructured annotation” — are inserted within the relevant `<bibl>` using the `<note>` element. I use `<note type="lexical">` to explain lexical features and `<note type="contextual">` to provide other explanatory information. In a future iteration of this project will incorporate `<note type="biographical">`. 

The primary use of such notes currently is to provide information about the object that cannot be conveyed by the hierarchy of the structured annotation alone. Articulated as `<note type="bibliographical">`, these notes operate much like `<person>`, in the sense that they function as a layer between `<listBibl>` and `<bibl>`. They differ, however, as they represent editorial additions to the structure of the text, while `<person>` is part of the text itself. 

One example of the latter involves the doubts I have expressed about whether Johann Adam Lonicerus (_Teucride Aneo Lonicero_ in the _Epítome_) translated J. H. van Linschoten’s _Itinerario_ to German, as León Pinelo asserts.[^9] Another is the explanation I provide regarding the various sixteenth-century editions of Ramusio’s _Navigationi et viaggi_.[^10] 

To call these bibliographical notes “unstructured” is not entirely accurate, as they often contain structured information that follows the logic explained above. I place them outside the category of “structured” annotation, however, because they involve a traditional note that interrupts the strict hierarchy I have established, with the note itself serving as a container for the structured information. 

## 2.0 Interface

The interface to display this prototype was first developed using TEI-Boilerplate.[^11] With assistance from Raffaele Viglianti, the edition was subsequently adapted to operate within the framework of _Scholarly Editing_. The menus and behaviors were recreated in that environment, and the CSS was reimplemented. This process resulted in various improvements to the interface and edition that were not contemplated in the preliminary version.

The interface includes two general display options. The user may select to view the text and apparatus in English or Spanish. “Hierarchical view” renders visual the text’s division into primary and subordinate objects, with each `<bibl type="orig">` with a subtype of “primary” appearing flush with the left margin and each nested `<bibl type="orig">` with a subtype of “other_edition,” “translation,” or “misc” indented a number of times that correspond to its place within that structure.[^12] 

The three categories of semantic markup can be highlighted. The contextual elements (person, place, date) can, as a group, be made to appear in bold. The various categories of bibliographical markup can be highlighted all at once or individually, and the same is true for the discursive markup. When showing the bibliographical and discursive markup, the interface employs a system of colors that correspond to the respective switches in the menu.[^13] Tooltips have also been added to the display of all three semantic categories to increase accessibility. 

The structured and unstructured annotations can also be revealed or hidden. Selecting any of the categories of structured annotation causes the edition to display with the same system of indentation that is invoked by the “heirarchical view” option. The individual categories of structured and unstructured annotation are highlighted with an array of colors similar to that mentioned above. Unstructured annotations will only display when the enclosing `<bibl>` is shown. For instance, a `<bibl>` indicating the editor’s source will only show if the enclosing `<bibl>`, such as a regularized version, is visible. 

## 3.0 Preparing the Text

This prototype edition is based on a copy of the text held by the John Carter Brown Library. I have chosen items from each of the four bibliotecas that I believe to be representative, although the selections here cannot be asssumed to present every scenario that could arise in a full edition. In preparing the text and translation, I have followed criteria that are designed to respect the idiosyncracies of the text while making both versions as accessible as possible to modern readers.

### 3.1 Editorial Criteria

I have taken an approach that is common today, if not standard, in the edition of Spanish-language texts from the sixteenth and seventeenth centuries. This involves regularizing punctuation, capitalization, and spelling according to modern practice, as long as, in the case of the latter, such modifications do not alter the phonology of the text. This entails standardizing the use of various combinations of letters — such as _b_ and _v_ —that had once represented differing sounds in Spanish but had ceased to do so by the era in question. It also means not introducing into the text phonological features that are posterior to its creation, such as the imposition of the learned consonant clusters (_pt_, _ct_, _mn_, _xp_, etc.) when they are not already present.[^14]

I apply special criteria to proper nouns. When names of people have forms that are common in Spanish today, I regularize when such changes can be carried out according to the logic explained above (_Rodriguez_ to _Rodríguez_, for instance). When an idiosyncratic spelling represents how a particular author’s name is still rendered, or might still be rendered today, I do not regularize. The same is generally true also of non-Hispanic names, which I reproduce as León Pinelo writes them (e.g., _Juan Hugon Linzcotan_). I handle place-names in the same fashion, regularizing when changes can be made without altering the sound of the text, but respecting the spelling of the original in all other cases.[^15]

I expand all abbreviations. This is straightforward in most cases, but with “imp.” and “m.s.”, some interpretation is involved. The former stands for _impreso_, which depending on context can be an adjective meaning _printed_ (as in _un libro impreso, una memoria impresa_) or a noun (_un impreso_), as shorthand for _printed book_. The same is true for _m.s._, which can represent an adjective meaning _handwritten_ (_un texto manuscrito, una historia manuscrita_)[^16] and also a noun (_un manuscrito_). My expansion of these abbreviations reflects my interpretation of the grammatical function of the words in each case.

I have not modernized the bibliographic data of items included in `<bibl>` elements that have a value for @subtype of “regularized”, “author_source”, “editor_source,” “other_edition,” and “digital_version”. I have instead reproduced punctuation and spelling as is (including the use of _u_ and _v_). I also respect the use of capital and lowercase letters, with one exception: when words in titles appear in all capital letters, I transcribe according to modern practice. I also do not modernize when citing from texts from the period, including _Autoridades_ and other dictionaries. Although I have not modernized titles, I have in some cases truncated them, inserting an elipsis to represent the portions removed.

I have regularized the division of words, except in the case of contracted forms that were common in the period, such as _deste_, _deste_, _dello_, y _della_. When a term is divided across a page break, I write the complete form on the first of the two pages. For instance, I modify _Hálla`<pb n="59"/>`se en memoria impresa_[^17] to _Hállase `<pb n="59"/>` en memoria impresa_.

I have spelled out numbers that can be expressed as one or two words and have written all others as numerals. I have preserved León Pinelo’s use of roman numerals for the headings of the _títulos_ but employ Arabic numerals elsewhere.

### 3.2 Approach to Translation

In preparing the English translation, I have striven for both readability and accuracy. When needing to choose between literal translations or versions that work better stylistically in English, I have opted for the latter, as long as the meaning of the passage remains consistent with the original. 

In cases where the antecedents of pronouns or the subjects of verbs are potentially unclear in the Spanish, I have supplied names explicitly in the translation, when I am able to do so. For instance, when referring to a translation done by Francisco de Herrera Maldonado, León Pinelo’s wording presents some ambiguity: “Adviértase que es la de Fernán Méndez Pinto, que tradujo de portugués en castellano, que queda puesta, y no otra que haya escrito”.[^18] I have attempted to render this in a fashion that provides clarity: “Be it known that it is the history of Fernán Méndez Pinto, which Herrera Maldonado translated from Portuguese into Spanish, that is registered in the present bibliography, and not any other that Herrera Maldonado might have written.”

The handling of person names again demands special consideration. I do not translate the bibliographer’s presentation of foreign names. For instance, when I read _Teucride Aneo Lonicero_ in the Spanish original,[^19] I write _Teucride Aneo Lonicero_, not _Johann Adam Lonicerus_ (nor _Teucrides Anneaus Lonicerus_ or any other form). I do so because I view León Pinelo’s own translations or Hispanized forms of foreign names to be idiosyncratic units of text that resist retranslation.[^20]

I handle León Pinelo’s presentation of the titles of foreign books in a similar fashion. In cases in which I believe the author regards such titles as actual titles, not descriptions, I reproduce his version in Spanish, providing my own English translation (not a corrected version of the title or a canonical English rendition) in parentheses. I do so because, as his titles are often not the exact titles of the works in question, literal translations of his versions into English would introduce another layer of distance from whatever the original titles actually are. In cases in which I believe León Pinelo regards the material in question as a description, I do translate, as I do not view such a block of text as a self-contained unit that must be handled with the same caution.[^21]

In general, I have not translated anything within the `<bibl type="add">` elements, a consideration that I believe is most pressing in the case of the subtype="reg" objects that provide the correct bibliographic information for the volume in question. I believe this material to have a documentary value from which translation would detract. Also, any unstructured prose notes within a `<bibl type="add">` are presented in both languages, as they are elsewhere in the text. 

León Pinelo uses a few terms in ways that merit clarification here. He employs _biblioteca_ (library) in the sense of an inventory or repertory of books. As Luigi Balsamo explains, this metaphorical usage was common at the time, with _biblioteca_ (or _bibliotheca_) being a precursor to the term bibliography, an expression that appeared toward the middle of the seventeenth century.[^22] While _repertory_, _inventory_, or _listing_ may be more exact translations, I opt for _library_ because I believe it captures the metaphorical value present in León Pinelo’s use. When he uses the term to refer to a specific bibliographic work, however, I generally use one of the former terms.[^23]

I have translated _alegar_, usually used in reference to an author mentioning a text unknown to León Pinelo, as _to claim the existence of_ (as in “En este libro alega su historia oriental”). I likewise have rendered _quedar puesto_ as _to be registered in the present bibliography_ (as in “es la de Fernán Méndez Pinto [...] que queda puesta, y no otra”).[^24]

As the term _la India_ was used in the period to refer to South Asia and East Asia generally, not just to the subcontinent, I translate it as _the Indies_ or _the East Indies_.

## Abbreviations Employed in This Edition
_Aut._ | Real Academia Española, _Diccionario de la lengua castellana_ (“_Autoridades_”), 1726–1739
</br>_DLE_  | Real Academia Española, _Diccionario de la lengua española_, 23rd edition, 2014 
</br>_CORDE_  | Real Academia Española, _Corpus diacrónico del español_
</br>s.v.   | _sub voce_ (in reference to dictionary entries)    

## Acknowledgments

For their support, assistance, and encouragement, I would like to thank the following individuals: Constanza López Baquero, Raffaele Viglianti, Andy Jewell, Amanda Gailey, Karthik Umapathy, Pablo Jauralde Pou, Jennifer Stertzer, Erica Cavanaugh, Hannah Alpert-Abrams, Heather Allen, Andrew Reynolds, Molly Hardy, and K. Anagnostou. I am grateful also to Robin Sheffield, who prepared the transcription on which this prototype is based, and Rachel Bennett, who carried out the first round of TEI-XML markup. I would like to thank as well the University of North Florida (UNF) Digital Humanities Institute; the UNF Center for Instruction and Research Technology; the UNF Department of Languages, Literatures and Cultures; the UNF Office of Research and Sponsored Programs; and the Colonial Section of the Latin American Studies Association. 

Document images are used courtesy of the John Carter Brown Library and are drawn from a digital facsimile available through [archive.org](https://archive.org/details/epitomedelabibli00lenp).

This project is dedicated to the memory of Lía Schwartz and Isaías Lerner. 

[^1]: Antonio de León Pinelo, _Epitome de la biblioteca oriental i occidental, nautica i geografica_ (Madrid: Juan Gonzalez, 1629). I present the title here without modification, but modernize elsewhere throughout.

[^2]: “3. Elements Available in All TEI Documents,” TEI P5: Guidelines for Electronic Text Encoding and Interchange, Text Encoding Initiative, version 4.2.2, April 9, 2021, [https://www.tei-c.org/release/doc/tei-p5-doc/en/html/CO.html](https://www.tei-c.org/release/doc/tei-p5-doc/en/html/CO.html).

[^3]: In the first of these modifications, I have altered the schema to allow `<seg>` to contain the elements that can appear inside `<bibl>`. I have done so in order to segment the content of `<bibl>` and thus create a mechanism for multilingual functionality. In other words, I am using `<seg>` to encode the parallel Spanish and English versions of León Pinelo’s text within each `<bibl>`. While this could also be accomplished by creating duplicate `<bibl>` elements for the Spanish and English version of each item, I have opted instead to segment the text within a solitary `<bibl>` in each case for three reasons. Most importantly, I believe that creating duplicate `<bibl>` elements merely for the purposes of multilingual encoding would interfere with my efforts to use TEI to express the content model I have designed as cleanly as possible. Alongside that conceptual concern lies a practical one, as the nesting of `<bibl>` elements (upon which the structured annotation of the text is based) would be more complex to implement if multiple copies of eachb parent element existed. Additionally, the creation of duplicate `<bibl>` elements would result in redundant `<pb>` elements when those fall within a `<bibl>`. The second customization involves enabling `<listBibl>` to contain `<person>`, a change I have made to address the problem of people being listed as entities at the same level as books, an issue we find, for instance, in Título XXVI (“Colectores de libros de Indias”) of the Western Library (see section 1.1. of this introduction). In truth, in order to fully express my concept model in TEI, one other customization would be necessary, as the schema would need to be modified to allow the nesting of `<listBibl>` within `<bibl>`. I have not implemented that change in this prototype, however, and instead have included the lists of child `<bibl>` elements inside parent `<bibl>` elements without intervening `<listBibl>` containers.


[^4]: I have not done the same for `<placeName>` and `<persName>`, since these are already separate elements from `<pubPlace>` and `<author>`, respectively. I mark up these contextual items only in the text of León Pinelo’s original entries (`<bibl type="orig">`). 

[^5]: For an example, see the English version of the Linschoten entry (54).

[^6]: Note that `<seg type="semantic" subtype="pub_status">` differs from `<seg type="semantic" subtype="state">` in the sense that the latter refers to the indication of an objective condition (the affirmation that an item exists in a handwritten or printed state), whereas the former points to a discursive act on the part of the bibliographer, through which he seeks to call attention to the unpublished state of a work.

[^7]: León Pinelo, _Epítome_, 60.

[^8]: When citing from bibliographical repertories that are organized in alphabetical order, I only include a locator number or other identifier or pointer in cases where such information might be necessary to find the item in question.

[^9]: León Pinelo, _Epítome_, 60.

[^10]: León Pinelo,  _Epítome_, 133.

[^11]: TEI-Boilerplate is the open-source tool that John Walsh and his team developed at Indiana University. See John Walsh, Grant Simpson, and Saeed Moaddeli, “About TEI Boilerplate,” accessed April 22, 2021, [https://dcl.ils.indiana.edu/teibp](https://dcl.ils.indiana.edu/teibp).

[^12]: For an example of how this nesting works, see the entry for Jacobo de Mayre on p. 90, which shows three levels of indentation.

[^13]: The colors employed derive from a scheme developed by Paul Tol for displaying qualitative data. See Paul Tol, “Qualitative Colour Schemes,” Paul Tol’s Notes: Color Schemes and Templates, accessed April 2, 2021, [https://personal.sron.nl/~pault/#sec:qualitative](https://personal.sron.nl/~pault/#sec:qualitative).

[^14]: For a more detailed explanation, see Clayton McCarl, “Introduction to ‘Avisos a pretendientes para Indias,’” _Scholarly Editing: The Annual of the Association for Documentary Editing_ 35 (2014), [http://scholarlyediting.org/2014/editions/intro.avisos.html](http://scholarlyediting.org/2014/editions/intro.avisos.html).

[^15]: The identities of the individuals referenced are clarified, when possible, and more canonical forms of their names provided, through the structured annotation.

[^16]: See, for example, León Pinelo’s reference to Bernal Díaz del Castillo’s text: _“Historia de la conquista de Nueva España, manuscrita....”_ (75).

[^17]: León Pinelo,  _Epítome_, 59-60.

[^18]: León Pinelo,  _Epítome_, 30.

[^19]: León Pinelo,  _Epítome_, 54.

[^20]: As above (see note 15), the identities of the individuals referenced are clarified through the structured annotation.

[^21]: As in the case of foreign person names, the volumes for which titles are not translated are identified, when possible, through the structured annotation.

[^22]: Luigi Balsamo, _Bibliography: History of a Tradition_ (Berkeley, CA: B. M. Rosenthal, 1990), 5. See also _Aut._, s.v. _bibliotheca_: “Se llaman tambien assi algunos libros, ù obras de algunos Autóres que han tomado el assunto de recoger y referir todos los Escritóres de una Nación que han escrito obras, y las que han sido, de que tenémos en España la singulár y tan celebrada de Don Nicolás Antonio”). 

[^23]: See, for instance, the entry for Antonio Possevino’s _Bibliothecae selectae_. León Pinelo, _Epítome_, 39.

[^24]: León Pinelo, _Epítome_, 30.
