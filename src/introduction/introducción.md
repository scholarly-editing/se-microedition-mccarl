---
path: "/"
lang: "es"
title: "Un prototipo para una edición digital del <em>Epítome de la biblioteca oriental y occidental, náutica y geográfica</em> (1629) de Antonio de León Pinelo"
---

El *Epítome de la biblioteca oriental y occidental, náutica y geográfica* (1629) de Antonio de León Pinelo se considera la obra fundacional de la bibliografía americanista. Durante casi cuatro siglos ha sido una fuente esencial para los que estudian las letras hispánicas y el mundo marítimo de la edad moderna temprana.[^1] No es solamente una herramienta de referencia, sino también un objeto singular en términos genéricos, pues combina una incipiente práctica bibliográfica con discursos políticos y autobiográficos.

A pesar de su importancia, ninguna edición moderna del *Epítome* se ha llevado a cabo. Este proyecto contribuye a la resolución de esta omisión, al presentar un prototipo para una versión digital. Es una edición que no solo procura transmitir el texto del *Epítome* sino también facilitar la interacción con él en dos modalidades: como objeto discursivo y como repositorio de datos. Esta funcionalidad se logra mediante el uso de un vocabulario personalizado para el marcado semántico y la adición de datos formalizados.

La estrategia editorial que emplea la edición se explica en detalle en mi artículo “El marcado semántico y la anotación estructurada de las bibliografías de la edad moderna temprana”, incluido en este mismo número de _Scholarly Editing_. Este ensayo esboza un modelo conceptual para entender el *Epítome* desde una perspectiva editorial y señala ejemplos del texto que figuran en este prototipo. El artículo también reflexiona sobre lo que podemos aprender de esta implementación provisional.

Ya que el estudio mencionado establece el marco teórico y el contexto más amplio del proyecto, esta introducción abarca únicamente asuntos pertinentes a la creación del prototipo. Describe la ejecución en TEI-XML del paradigma que el artículo propone y repasa el desarrollo de la interfaz. Asimismo, explica los criterios de la edición y la traducción.

El prototipo tiene como propósito principal ser una herramienta para visualizar el modelo conceptual. Por lo tanto, está limitado en términos de alcance y funcionalidad. Contempla solamente la sección bibliográfica del *Epítome*, sin tomar en cuenta los textos preliminares en prosa y en verso. Incluye mecanismos para el análisis de los aspectos contextuales —como, por ejemplo, las referencias a lugares y a personas— pero enfatiza sobre todo la realización de un sistema para marcar y anotar los objetos bibliográficos en sí. La limitación más notable tal vez sea que la interfaz solo permita la interacción con el *Epítome* en su formato lineal original, sin posibilitar la consulta ni la visualización del texto de manera cronológica ni geográfica, ni según otros criterios. Anticipo enfrentarme a aquellas necesidades, y a otras, en una fase futura del proyecto.

## 1.0 El marcado

Este proyecto emplea el TEI-XML siguiendo las pautas de la versión P5. La mayoría de los elementos que utiliza figuran en el apartado “Bibliographic Citations and References” (Citas y referencias bibliográficas) dentro la sección “Elements Available in all TEI Documents” (Elementos que están disponibles en todos los documentos TEI).[^2] No he modificado el esquema con excepción de dos cambios menores.[^3]

### 1.1 El marcado del texto

El modelo conceptual que subyace a este proyecto consta principalmente de dos tipos de objetos: listas y libros. La primera categoría incluye cualquier enumeración, y la segunda abarca todo tipo de objeto bibliográfico. Siguiendo este paradigma, los componentes básicos de la implementación son las listas, articuladas ​​como `<listBibl>`, y los libros, expresados ​​como `<bibl>`.

El modelo permite el anidamiento de los dos tipos de objetos entre sí. Las listas pueden contener libros y también otras listas. Los libros también pueden comprender tanto listas como libros, una circunstancia que se basa en la idea de que el objeto bibliográfico funciona como una especie de contenedor conceptual.

En este prototipo, el elemento de alto nivel es un `<listBibl>`, que corresponde a la sección bibliográfica del *Epítome*. Ese `<listBibl>` a su vez encierra cuatro más, uno para cada una de las principales divisiones, o *bibliotecas*, del libro de León Pinelo. Cada subdivisión geográfica o temática —o *título*, como los llama el bibliógrafo— dentro de cada una de las cuatro bibliotecas también es un `<listBibl>`.

Dentro de aquellas subdivisiones, cada entrada principal (la primera versión que se menciona a propósito de cualquier obra) aparece como un `<bibl>` con un valor de *orig* (original) para `@type` (tipo) y *primary* (principal) para `@subtype` (subtipo). Dentro de ese `<bibl type="orig" subtype="primary">` figuran los objetos adicionales que León Pinelo enumera respecto a ese registro, cada uno como un `<bibl type="orig">` y con un valor para `@subtype` de uno de los siguientes valores: *other\_edition, translation*, o *misc*.[^4]

Dentro de cada `<bibl type="orig">`, marco la información que corresponde a la referencia bibliográfica, que incluye los siguientes elementos: `<author>`, `<editor>`, `<title>`, `<pubPlace>`, `<publisher>`, y `<date>`. Utilizo `<textLang>` para señalar las referencias al idioma en que se escribe un texto, y `<dim>` para el formato de un volumen impreso (*cuarto, folio*, etc.). La división de los elementos `<bibl type="orig">` en los campos que los constituyen sigue el formato a veces imprevisible del texto, y por lo tanto es necesariamente irregular y refleja decisiones subjetivas.

Empleo los elementos que corresponden a los campos bibliográficos de manera convencional, con tres excepciones. Para `<title>`, uso `@type` para señalar cuando el material en cuestión no es un título verdadero sino más bien una descripción (_type="desc"_), una situación que es frecuente en el *Epítome*.[^5] Indico un valor de *pub* para `@type` en todas las fechas de publicación, para diferenciarlas de las fechas contextuales. Marco las menciones del estado de un texto (como manuscrito o impreso) con `<seg type="semantic" subtype="state">`.[^6]

En cada `<bibl type="orig">` uso `@xml:id` para asignar un identificador único, empleando el apellido del autor como punto de partida para determinar aquel valor. Uso formas canónicas cuando es posible (por ejemplo, para *Linzcotan*, uso *xml:id="linschoten"*),[^7] pero empleo las versiones de León Pinelo en los casos en que el autor en cuestión no puede ser identificado (para *fray Gaspar*, por ejemplo, uso *xml:id ="gaspar"*).[^8] Cuando varios autores tienen apellidos idénticos, agrego la primera inicial del nombre. Cuando se incluye más de un trabajo de un autor, añado el año, y cuando dos o más corresponden a un mismo año, incorporo una palabra clave de cada título (por ejemplo, *xml:id="herreraMaldonado1620Historia"* y *xml:id="herreraMaldonado1620Epitome"*).[^9] Agrego *Ed*, *Trans* o *Misc*, seguido por el año, para identificar las ediciones y traducciones que se relacionan con la entrada principal (como _xml:id="linschotenTrans1599"_). Cuando más de una traducción se publicó en un mismo año, incluyo el idioma (_xml:id="linschotenTrans1599Latin"_ y _xml:id="linschotenTrans1599German"_).[^10] Empleo este sistema de identificadores que son fácilmente legibles para simplificar el proceso de crear las referencias cruzadas.

He usado `@xml:lang` para indicar el idioma de cada componente de la edición. Todo el material que corresponde al texto original aparece dentro de elementos `<seg xml:lang="es">`, al igual que las versiones en español de todos los elementos del aparato crítico. La traducción del texto, junto con todos los aspectos del aparato en inglés, figuran dentro de elementos `<seg xml:lang="en">`. Los elementos `<seg xml:lang="es">` y `<seg xml:lang="en">` coinciden en el elemento `<bibl>` respectivo, o cualquier otro objeto que los contenga.

### 1.2 El marcado suplementario

Por *marcado suplementario* me refiero la codificación que no sirve principalmente para organizar y representar el texto mismo, sino para proveer mecanismos para su análisis e interpretación. Describo aquí dos tipos: marcado semántico y anotación. El primero se divide en tres categorías: marcado contextual, bibliográfico, y discursivo. El segundo engloba dos tipos de anotación: estructurada y no estructurada.

Por *marcado contextual*, me refiero a la identificación de información que se relaciona con las circunstancias históricas de los `<bibl type="orig">` y los comentarios del bibliógrafo sobre ellos. He marcado tres tipos: nombres de personas, nombres de lugares, y fechas. Los he codificado como `<persName>`, `<placeName>`, y `<date>`, respectivamente. Para `<placeName>` asigno valores para `@type` siguiendo una taxonomía fija que incluye *settlement* (asentamiento), *nation* (nación), *region* (región) y *topographical* (topográfico). Para `<date>` utilizo un valor para `@type` de *contextual*, para diferenciar aquellas fechas de las de publicación.[^11]

Al señalar el *marcado bibliográfico* como categoría, me refiero a la codificación de los campos que componen un `<bibl type="orig">`. Ya examiné este proceso en la sección 1.1 como parte de la descripción del texto, ya que en un nivel básico, aislar entidades como `<author>`, `<title>`, y `<pubPlace>` es un aspecto de indicar lo que ya está presente. Son unidades estructurales que forman parte del conjunto de una referencia bibliográfica, así como las páginas, capítulos, párrafos, etc., conforman el entablado de un libro.

Además de tener tal función descriptiva, sin embargo, el marcado bibliográfico también desempeña un rol interpretativo, ya que no solo tiene repercusiones para la estructura del texto, sino también para su significado. El etiquetado de elementos como `<author>`, `<title>` y `<pubPlace>` es, en parte, un acto exegético. Este hecho es evidente sobre todo en las decisiones subjetivas que deben tomarse con respecto a `<title>`.

Por *marcado discursivo*, me refiero a las siete categorías retóricas que establezco en “Marcado semántico y anotación estructurada de las primeras bibliografías modernas”. Para delinear las secciones de texto correspondientes, utilizo `<seg>` con un valor de *semantic* (semántico) para `@type` y un valor para `@subtype` que describe la función discursiva en cuestión. Como explico en el artículo, un pasaje en el que León Pinelo expresa preocupación por la veracidad de su información, o la de otros, se marca con *doubt* (duda). El valor de *criticism* (crítica) se utiliza para textos en los que el bibliógrafo parece criticar el trabajo o las ideas de otros autores, o articular juicios sobre la sociedad de manera más amplia. Cuando León Pinelo rectifica errores que figuran en otros libros, los pasajes en cuestión se denominan *correction* (corrección). Las secciones en las que celebra a otros autores e individuos de influencia están marcadas como *praise* (elogio), y *self\_promotion* (autopromoción) se utiliza cuando el autor busca promover su propia reputación o la de su trabajo. Un valor de *pub\_status* (estado de publicación) señala ocasiones en que León Pinelo llama la atención sobre la condición inédita de ciertos textos, a menudo insinuando o indicando de forma directa que se deben tomar medidas para remediar la situación.[^12] Los pasajes se designan como *witness* (testigo) cuando el bibliógrafo atestigua ciertos hechos, generalmente a propósito de haber visto personalmente un manuscrito o un libro difícil de encontrar en una colección particular.

La anotación estructurada del texto se logra al ampliar el paradigma de listas y objetos bibliográficos. Se trata de anidar dentro de un `<bibl type="orig">` un `<listBibl>` que contiene elementos bibliográficos adicionales que sirven para explicarlo o contextualizarlo. Cada uno existe como un `<bibl>` con un valor de *add* (agregar) para `@type,` y un valor para `@subtype` que refleja la naturaleza o función del objeto insertado.

Un elemento `<bibl type="add">` con un valor de *author\_source* (fuente del autor) para `@subtype` señala el libro de que León Pinelo recopila su información, cuando es posible identificarlo. Tales fuentes suelen ser otros repertorios bibliográficos, además de algunas obras historiográficas. Utilizo `<citedRange>` para indicar el lugar dentro de aquella fuente de donde León Pinelo extrae su información, cuando se puede determinar. De igual forma, empleo `<q>` para reproducir el pasaje que León Pinelo cita o del que probablemente recoge los datos. Cuando conozco o puedo especular con alguna certeza sobre la fuente, a su vez, de la fuente de León Pinelo, anido otro `<bibl type="add" subtype="author_source">`.[^13]

Un elemento `<bibl type="add">` con un valor de *regularized* (regularizado) para `@subtype` presenta la información bibliográfica completa del objeto que menciona León Pinelo. Tomo los datos de una copia del propio libro cuando es posible. En las ocasiones en que es necesario recurrir a alguna fuente secundaria, añado otro `<bibl type="add">` con un valor de *editor\_source* (fuente del editor) para `@type` dentro del respectivo elemento `<bibl type="add" subtype="regularized">`. Uso `<citedRange>` para señalar la ubicación de la información en aquella fuente, según corresponda,[^14] y `<q>` para incluir una cita literal del material relevante, cuando es posible.

Mientras que los elementos `<bibl type="add">` con subtipos de *author\_source, regularized* y *editor\_source* están dirigidos a explicar y aclarar las referencias de León Pinelo, dos tipos adicionales de `<bibl type="add">` sirven para facilitar el acceso del usuario a los textos en cuestión. El primero, con un subtipo de *digital\_version* (versión digital), apunta a una edición o facsímil en línea.[^15] El segundo, con un subtipo de *modern\_edition* (edición moderna), contiene la información de una edición reciente del texto. Un `<bibl type="add">` puede contener varias instancias de elementos `<bibl>` subordinados con subtipos de *modern\_edition* y *digital\_version*.

Cuando el objeto que abarca un `<bibl type="add">` no lleva de forma explícita el nombre del autor, incluyo una versión estandarizada del nombre entre corchetes. Tal suele ser el caso, por ejemplo, de obras reunidas en colecciones como las de Giovanni Battista Ramusio y Theodor de Bry. Cuando enumero esas colecciones en su totalidad, utilizo el nombre del editor como el individuo responsable, colocándolo entre corchetes si no se indica explícitamente en el libro o los libros en cuestión, seguido de *ed*.[^16]

Cuando los datos completos para un elemento en un `<bibl type="add">` ya se han provisionado en otro lugar, no ingreso la información de manera redundante, sino que proporciono una referencia cruzada. Utilizo `<ref>` para apuntar al respectivo `<bibl type="add" subtype="regularized">`, excepto cuando la referencia es a la entrada de León Pinelo, en cuyo caso enlazo al `<bibl type="orig">` correspondiente.

Las entradas del Título XXVI (“Colectores de libros de Indias”) de la Biblioteca Occidental enumeran personas, no libros, y por lo tanto requieren un acercamiento especial. El modelo conceptual construido a base de listas y libros solo incluye al ser humano como un elemento subordinado dentro de `<bibl>` (como `<author>`, `<editor>`, etc.), no como un tipo de objeto que pueda existir en paralelo con `<bibl>`. El título mencionado rompe con ese paradigma, y como solución provisional, marco aquellas entradas con `<person>`. Al hacerlo, empleo `<person>` como una especie de capa intermedia entre `<listBibl>` y `<bibl>`.

La nomenclatura para xml:id en los elementos `<bibl type="add">` con subtipos de *editor\_source*, *author\_source*, *modern\_edition* y *digital\_version* sigue el patrón de las entradas `<bibl type="orig">` (ver la sección 1.1), dado que todos son puntos de partida para la anotación estructurada. Al asignar un valor para `@xml:id` en el elemento `<bibl type="add" subtype="regularized">` que corresponde a cualquiera de ellos, agrego *Reg*.[^17]

Las notas tradicionales en prosa, a las que me refiero como *anotaciones no estructuradas*, se insertan dentro del `<bibl>` relevante utilizando el elemento `<note>`. Utilizo `<note type="lexical">` (léxica) para explicar aspectos relacionados con el lenguaje y `<note type="contextual">` (contextual) para ofrecer otra información de índole histórica. La mayoría de las notas que se encuentran actualmente en el prototipo son `<note type="bibliographical">` (bibliográfica), y su objetivo es proporcionar explicaciones que la jerarquía de la anotación estructurada no puede comunicar por sí sola.[^18]

Llamar a las notas bibliográficas *no estructuradas* no es del todo exacto, ya que a menudo contienen información formalizada expresada a través de elementos `<listBibl>` y `<bibl>`, según los patrones ya descritos. Enmarco estas notas dentro de la categoría de anotación *estructurada,* sin embargo, porque consisten en una nota tradicional que interrumpe la jerarquía establecida, con la nota misma funcionando como un contenedor para la información. En ese sentido, desempeñan un rol parecido al de `<person>`, ya que representan una capa entre elementos `<listBibl>` y `<bibl>`. Al mismo tiempo, difieren de `<person>` ya que son adiciones editoriales, mientras el contenido básico de `<persona>` es parte del texto en sí.

## 2.0 La interfaz

Desarrollé una versión preliminar de la interfaz utilizando TEI-Boilerplate.[^19] Con la ayuda de Raffaele Viglianti, adapté posteriormente la edición para funcionar en la plataforma de *Scholarly Editing*. Volvimos a crear los menús y los comportamientos, y a implementar el CSS en aquel entorno. Este proceso resultó en varias mejoras en la edición y su presentación visual que no se habían contemplado en la versión anterior.

La interfaz incluye dos opciones generales que tienen que ver con el idioma y el formato. La primera permite al usuario ver la edición, con todos sus componentes, en inglés o el español. La segunda, llamada *vista jerárquica*, hace que sea visible la división del texto en objetos primarios y subordinados, con cada `<bibl type="orig" subtype="primary">` alineado con el margen izquierdo y cada `<bibl>` subordinado sangrado el número de veces que corresponde a su lugar dentro de la estructura de la entrada.[^20]

Se pueden resaltar en la interfaz las tres categorías de marcado semántico. Los elementos contextuales (persona, lugar, fecha) pueden, como grupo, aparecer en negrita. Las diversas categorías de marcado bibliográfico se pueden resaltar todas a la vez o individualmente, y lo mismo ocurre con el marcado discursivo. Al mostrar el marcado bibliográfico y discursivo, la interfaz emplea un sistema de colores que corresponden a los respectivos selectores del menú.[^21] Descripciones emergentes (*tooltips*) sobre las tres categorías semánticas sirven para incrementar el grado de accesibilidad.

Las anotaciones estructuradas y no estructuradas también se pueden revelar u ocultar. Al seleccionar cualquiera de las categorías de anotación estructurada, la edición se muestra con el mismo sistema de sangría que se invoca con la opción v*ista jerárquica*. Las categorías individuales de anotaciones estructuradas y no estructuradas se resaltan con una gama de colores similar a la mencionada anteriormente.[^22]

## 3.0 La preparación del texto

Esta edición prototipo se basa en una copia del texto que está en poder de la Biblioteca John Carter Brown. He elegido elementos de cada una de las cuatro bibliotecas que considero representativos, aunque seguramente no presenten todos los casos que podrían surgir en una edición completa. Al preparar el texto y la traducción, he seguido criterios diseñados para respetar la idiosincrasia del texto y hacer que ambas versiones sean accesibles para los lectores modernos.

### 3.1 Los criterios editoriales

Empleo un acercamiento que es común, si no estándar, hoy en día en la edición de textos en español de los siglos XVI y XVII. Se trata de regularizar de acuerdo con la práctica moderna la puntuación, el uso de mayúsculas, y la ortografía, siempre que, en el caso de la tercera categoría, dichas modificaciones no alteren la fonología del texto. Esto implica la regularización del uso de varias combinaciones de letras, como *b* y *v*, que alguna vez representaron diferentes sonidos en español, pero que dejaron de hacerlo en la época en cuestión. También significa no introducir en el texto características fonológicas posteriores a su creación, como la imposición de los grupos de consonantes cultos (*pt, ct, mn, xp,* etc.) cuando ya no están presentes.[^23]

Aplico criterios especiales a los nombres propios. Cuando los nombres de las personas tienen formas que son comunes en el español de hoy, regularizo cuando tales cambios se pueden llevar a cabo de acuerdo con la lógica explicada arriba (de *Rodriguez* a *Rodríguez*, por ejemplo). Cuando una ortografía idiosincrásica representa la manera en que se sigue representando el nombre de un autor en particular, o cómo todavía se podría representar, no regularizo. Lo mismo ocurre generalmente con los nombres no hispanos, que reproduzco tal como los escribe León Pinelo (por ejemplo, *Juan Hugon Linzcotan*). Manejo los topónimos de la misma manera, regularizándolos cuando se pueden hacer cambios sin alterar el sonido del texto, pero respetando la ortografía del original en las demás ocasiones.[^24]

Resuelvo todas las abreviaturas. Es un acto sencillo en la mayoría de los casos, pero alguna interpretación es necesaria a propósito de *imp.* y *ms.* La primera significa *impreso*, que según el contexto puede ser un adjetivo (*un libro impreso, una memoria impresa*) o un sustantivo (*un impreso*). Lo mismo ocurre con *ms.*, que puede representar un adjetivo (*un texto manuscrito, una historia manuscrita*)[^25] y también un sustantivo (*un manuscrito*). La expansión de estas abreviaturas refleja mi interpretación de la función gramatical de las palabras en cada caso, asegurando la concordancia de género y número con el antecedente cuando se trata de un adjetivo.

No modernizo los datos bibliográficos de los elementos que figuran en los `<bibl>` que tienen los siguientes valores para `@subtype:` *regularized, author\_source, editor\_source, modern\_edition* y *digital\_version*. Reproduzco la puntuación y la ortografía tal cual (incluido el uso de *u* y *v*). También respeto el uso de mayúsculas y minúsculas, con una excepción: cuando los títulos aparecen completamente en mayúsculas, transcribo de acuerdo con la práctica moderna. Aunque no regularizo los títulos, en algunos casos los trunco, insertando tres puntos para representar las partes que elimino. No modernizo al citar textos de la época, incluyendo *Autoridades* y otros diccionarios.

He regularizado la división de palabras, salvo en el caso de formas contraídas que eran habituales en la época, como *deste, dello*, y *della*. Cuando un término se divide entre dos páginas, escribo la palabra completa en la primera.[^26]

Deletreo los números que se pueden expresar como una o dos palabras, y escribo todos los demás con cifras. Conservo el uso que hace León Pinelo de números romanos para los varios títulos de cada biblioteca, pero empleo números arábigos en otros lugares.

### 3.2 La aproximación a la traducción

Al preparar la traducción al inglés, he procurado la legibilidad tanto como la precisión. He optado en todos los casos por traducciones que suenan bien en inglés por encima de versiones más literales, siempre que el significado del pasaje sea consistente con el original.

En los casos en que los antecedentes de los pronombres o los sujetos de los verbos son potencialmente confusos en español, procuro ser más explícito en la traducción. Por ejemplo, al referirse a una traducción realizada por Francisco de Herrera Maldonado, la redacción de León Pinelo presenta cierta ambigüedad: “Adviértase que es la de Fernán Méndez Pinto, que tradujo de portugués en castellano, que queda puesta, y no otra que haya escrito”.[^27] He tratado de traducir esto de una manera que brinde claridad: “Be it known that it is the history of Fernán Méndez Pinto, which Herrera Maldonado translated from Portuguese into Spanish, that is registered in the present bibliography, and not any other that Herrera Maldonado might have written”.

El manejo de los nombres de las personas nuevamente exige una consideración especial. En particular, no altero en la traducción la presentación del bibliógrafo de nombres extranjeros. Considero que las traducciones de León Pinelo de nombres extranjeros —e incluso las formas de ellos cuyo uso era más general en español— son unidades idiosincrásicas que se resisten a la re-traducción. Por ejemplo, cuando leo *Teucride Aneo Lonicero* en el original en español,[^28] escribo *Teucride Aneo Lonicero*, no *Johann Adam Lonicerus* (ni *Teucrides Anneaus Lonicerus* ni cualquier otra forma).[^29]

Me aproximo de manera similar a la presentación de León Pinelo de los títulos de libros extranjeros. Cuando me parece que el autor entiende el material en cuestión como una descripción y no un título de verdad, lo traduzco. Sin embargo, cuando creo que el autor considera que tales títulos son títulos reales en vez de descripciones, procedo con más precaución, ya que en tal caso se trata de un bloque de texto con una integridad o autonomía que debe respetarse. Reproduzco en la traducción la versión en español, proporcionando entre paréntesis mi propia traducción al inglés (no una versión corregida del título ni una versión canónica en inglés).[^30]

En general, no traduzco los contenidos de los elementos `<bibl type="add">`, y sobre todo en el caso de los objetos `<bibl type="add" subtype="reg">`, ya que detallan la información bibliográfica correcta para el volumen en cuestión. Se trata de material que tiene un valor documental al que el acto de traducir le restaría valor. La excepción son las notas en prosa (las anotaciones *no estructuradas*) que se presentan en ambos idiomas dentro de un `<bibl type="add">`, al igual que en otras partes del texto.

León Pinelo usa algunos términos de maneras que merecen aclaración aquí. Emplea *biblioteca* en el sentido de *inventario* o *repertorio de libros*. Como explica Luigi Balsamo, tal uso metafórico era común en la época, siendo *biblioteca* (o *bibliotheca*), un precursor de *bibliografía*, que apareció a mediados del siglo XVII.[^31] Si bien *repertorio, inventario* o *listado* serían traducciones más exactas, opto por *biblioteca* porque creo que capta el valor figurado que está presente en el uso de León Pinelo.[^32]

El bibliógrafo emplea *alegar* con en sentido de *afirmar la existencia de* (como en “En este libro alega su historia oriental”), normalmente en el contexto de señalar que un autor menciona un texto que León Pinelo desconoce o de cuya existencia duda. Traduzco *quedar* *puesto* como *estar* *inscrita en la presente bibliografía* (como en “es la de Fernán Méndez Pinto \[\...\] que queda puesta, y no otra”).[^33] Como el término *la India* se usaba en el período para referirse al sur y este de Asia en general, no solo al subcontinente, lo traduzco como *las Indias* o *las Indias Orientales*.

## Abreviaturas empleadas en esta edición
*Aut*.: Real Academia Española, *Diccionario de la lengua castellana* (“*Autoridades*”), 1726-1739
</br>*DLE*: Real Academia Española, *Diccionario de la lengua española*, 23.ª edición, 2014
</br>*CORDE*: Real Academia Española, *Corpus diacrónico del español*
</br>s.v.: _sub voce_ (en referencia a las entradas de los diccionarios)

## Agradecimientos

Por su apoyo y ayuda, quiero agradecer a Constanza López Baquero, Raffaele Viglianti, Andy Jewell, Amanda Gailey, Pablo Jauralde Pou, Karthik Umapathy, Jennifer Stertzer, Erica Cavanaugh, Hannah Alpert-Abrams, Heather Allen, Andrew Reynolds, Carl Stahmer, Molly Hardy y K. Anagnostou. Me gustaría agradecer también al UNF Digital Humanities Institute; UNF Center for Instruction and Research Technology; UNF Department of Languages, Literatures and Cultures; UNF Office of Research and Sponsored Programs; y la Sección Colonial de la Asociación de Estudios Latinoamericanos (LASA).

Robin Sheffield preparó la transcripción en que se basa este prototipo, y Rachel Bennett llevó a cabo la primera fase de marcado TEI-XML.

Las imágenes se utilizan por cortesía de la Biblioteca John Carter Brown y se extraen de un facsímil digital que está disponible a través de [archive.org](https://archive.org/details/epitomedelabibli00lenp).

Este proyecto está dedicado a la memoria de Lía Schwartz e Isaías Lerner.

[^1]: León Pinelo, *Antonio de. Epitome de la biblioteca oriental i occidental, nautica i geografica*. Madrid: Juan Gonzalez, 1629. No modifico el título aquí, pero sí en todas las otras ocasiones.

[^2]: “Elements Available in all TEI Documents”, TEI P5: Guidelines for Electronic Text Encoding and Interchange, Text Encoding Initiative, Version 4.2.2. 9 April 2021, https://www.tei-c.org/release/doc/tei-p5-doc/en/html/CO.html.

[^3]: La primera modificación se trata de permitir que `<seg>` abarque los elementos que pueden aparecer dentro de `<bibl>`, un cambio que he efectuado para lograr la segmentación del contenido de `<bibl>` y así posibilitar la funcionalidad multilingüe. En otras palabras, uso `<seg>` para codificar las versiones paralelas en español e inglés del texto de León Pinelo dentro de cada `<bibl>`. En teoría, el mismo resultado podría lograrse mediante la creación de elementos `<bibl>` duplicados para las versiones en español e inglés de cada elemento, pero he optado por segmentar el texto dentro de cada `<bibl>` por tres razones. Primero, duplicar los elementos `<bibl>` complicaría mis esfuerzos por usar la TEI para expresar en una forma sencilla el modelo de contenido que he diseñado. Junto a esa consideración conceptual hay otra de índole práctica, ya que el anidamiento de elementos `<bibl>`, en que se basa la anotación estructurada del texto, sería más difícil de implementar si existieran múltiples instancias de los elementos principales. Por último, la creación de elementos `<bibl>` duplicados también resultaría en elementos `<pb>` redundantes cuando aquellos se encuentran dentro de un elemento `<bibl>`. La segunda modificación radica en permitir que `<listBibl>` contenga `<person>`, y es una manera de enfrentarme al problema de la inclusión de personas como entidades al mismo nivel de los libros, una situación que encontramos, por ejemplo, en el título XXVI (“Colectores de libros de Indias”) de la Biblioteca Occidental (véase la sección 1.1 de esta introducción). En verdad, para expresar completamente mi modelo conceptual en TEI, sería necesaria otra personalización, ya que el esquema tendría que modificarse para permitir el anidamiento de `<listBibl>` dentro de `<bibl>`. No he implementado ese cambio en este prototipo, sino que he incluido las listas de elementos secundarios `<bibl>` dentro de los elementos principales `<bibl>` sin que se interponga `<listBibl>` como contenedor.


[^4]: La última categoría (*misc*) de carácter general sirve para cualquier elemento relacionado que no sea edición o traducción. Ver, por ejemplo, las anotaciones de Ceporino al texto de Dionysius Periegetes. Léon Pinelo, _Epítome_, 154.

[^5]: En muchos casos, la naturaleza de un título (ya sea un título real o una descripción) no está clara y, en tales casos, opto por tratar lo que ofrece León Pinelo como un título de verdad. Lo mismo es cierto en los casos en que el bibliógrafo parece entender claramente que lo que presenta es un título, pero no es el título real o exacto de la obra en cuestión. En otras palabras, considero que un título es un título cuando parece que el bibliógrafo lo ha entendido de esa manera, incluso cuando no es el título correcto o completo de esa obra real en cuestión, y también cuando la existencia de la obra referida no es cierta.

[^6]: Empleo `<seg>`, ya que P5 no proporciona un elemento para marcar el formato del texto dentro de `<bibl>`.

[^7]: Léon Pinelo, _Epítome_, 54.

[^8]: Léon Pinelo, _Epítome_, 58.

[^9]: Los ejemplos corresponden a los siguientes textos de Francisco de Herrera Maldonado: *Historia oriental de las peregrinaciones de Fernan Mendez Pinto* y *Epitome historial del Reyno de la China*. Véase Léon Pinelo, _Epítome_, 11 y 29-30. 

[^10]: Léon Pinelo, _Epítome_, 54.

[^11]: No he hecho lo mismo a propósito de `<placeName>` y `<persName>`, ya que estos ya son elementos separados de `<pubPlace>` y `<autor>`, respectivamente. En la versión en inglés de cada `<bibl type="orig">`, cuando los títulos se traducen entre paréntesis, marco los elementos contextuales solo en el título traducido, ya que el usuario puede consultar el español original para el marcado correspondiente. Para un ejemplo, véase la versión en inglés de la entrada de Linschoten. Léon Pinelo, _Epítome_, 54.

[^12]: `<seg type="semantic" subtype="pub_status">` difiere de `<seg type="semantic" subtype="state">` en el sentido de que el último se refiere a la indicación de una condición objetiva (la afirmación de que un elemento existe en un estado escrito a mano o impreso), mientras que el primero apunta a un acto discursivo por parte del bibliógrafo, en el que busca llamar la atención sobre el estado inédito de una obra.

[^13]: Véase, por ejemplo, el libro de “Pedro Aliosio”, para el que León Pinelo cita a Michaele Routartio, que por su parte parece seguir la obra de Antonio Possevino. Léon Pinelo, _Epítome_, 60.

[^14]: Al citar repertorios bibliográficos que están organizados por orden alfabético, solo incluyo un número de localizador u otro identificador en los casos en que dicha información pueda ser necesaria para encontrar el artículo indicado.

[^15]: Cuando los datos bibliográficos para esa versión son idénticos a la información en el respectivo `<bibl type="add" subtype="regularized">`, incluyo solo el nombre del contenedor (Google Books, etc.), usando `<ref>` para enlazar a la dirección web correspondiente. Si es diferente, incluyo la información bibliográfica completa.

[^16]: He manejado de esta manera, por ejemplo, las entradas que corresponden al *Novus orbis* de Gaspar Barlaeus.

[^17]: Por ejemplo, el valor de `@xml:id` para el libro de Linschoten es *linschoten* y el valor de `@xml:id` en la entrada regularizada correspondiente es *linschotenReg*. Asimismo, el apartado para la versión regularizada de la traducción de 1599 tiene un valor para `@xml:id` de *linschoten1599TransReg*.

[^18]: Un ejemplo son las dudas que expreso sobre si Johann Adam Lonicerus (*Teucride Aneo Lonicero* en el *Epítome*) tradujo el *Itinerario* de Linschoten al alemán, como afirma León Pinelo. _Epítome_, 60. Otro ejemplo es la explicación que escribo sobre las varias ediciones del siglo XVI del *Navigationi et viaggi* de Ramusio. León Pinelo, _Epítome_,133. Una versión futura del proyecto incorporará *biographical* (biográfica) como otro valor posible para `@type` en `<note>`.

[^19]: TEI-Boilerplate es la herramienta de código abierto que John Walsh y su equipo desarrollaron en la Universidad de Indiana. Véase John Walsh, Grant Simpson y Saeed Moaddeli, “About TEI Boilerplate”, consultado el 22 de abril de 2021, https://dcl.ils.indiana.edu/teibp.

[^20]: Para ver un ejemplo de cómo funciona este anidamiento, véase la entrada de Jacobo de Mayre (90), que muestra tres niveles de sangría.

[^21]: Los colores empleados derivan de un esquema desarrollado por Paul Tol para la visualización de datos cualitativos. Véase Paul Tol, “Qualitative Colour Schemes”, Notas de Paul Tol: Colour Schemes and Templates, consultado el 2 de abril de 2021, https://personal.sron.nl/\~pault/\#sec:qualitative.

[^22]: Las anotaciones no estructuradas solo se muestran cuando está visible el `<bibl>` correspondiente. Por ejemplo, un `<bibl>` que indica la fuente del editor solo se muestra si el `<bibl>` relevante —como, por ejemplo, una versión regularizada— se ha relevado.

[^23]: Para una explicación más detallada, vea Clayton McCarl, Introducción a “Avisos a pretendientes para Indias”, *Scholarly Editing* 35 (2014), [http://scholarlyediting.org/2014/editions/intro. avisos.html].

[^24]: Las identidades de los individuos mencionados se aclaran, cuando es posible, a través de la anotación estructurada.

[^25]: Véase, por ejemplo, la referencia de León Pinelo al texto de Bernal Díaz del Castillo: “Historia de la conquista de Nueva España, _manuscrita_\... ”. Léon Pinelo, _Epítome_,  75, énfasis mío.

[^26]: Por ejemplo, modifico *Hálla `<pb n="59" />` se en memoria impresa* a *Hállase `<pb n="59" />` en memoria impresa*. Léon Pinelo, _Epítome_, 59-60.

[^27]: Léon Pinelo, _Epítome_, 30.

[^28]: Léon Pinelo, _Epítome_, 54.

[^29]: Como se mencionó en la sección 3.1 Los criterios editoriales, las identidades de los individuos mencionados se aclaran, cuando es posible, a través de la anotación estructurada.

[^30]: Como en el caso de los nombres de las personas (ver el párrafo previo), las identidades de los volúmenes cuyos títulos no se traducen se aclaran a través de la anotación estructurada en todos los casos en que es tal identificación es posible.

[^31]: Luigi Balsamo, *Bibliography: History of a Tradition* (Berkeley, Calif: B.M. Rosenthal, 1990), 5. Véase también *Aut*., s.v. *bibliotheca*: “Se llaman también assi algunos libros, ù obras de algunos Autóres que han tomado el assunto de recoger y referir todos los Escritóres de una Nación que han escrito obras, y las que han sido, de que tenémos en España la singulár y tan celebrado de Don Nicolás Antonio”.

[^32]: Cuando León Pinelo usa el término *biblioteca* para referirse a una obra bibliográfica específica, generalmente uso uno de los términos anteriores. Véase, por ejemplo, la entrada de *Bibliothecae selectae* de Antonio Possevino. Léon Pinelo, _Epítome_, 39.

[^33]: Léon Pinelo, _Epítome_, 30.
 

  [http://scholarlyediting.org/2014/editions/intro. avisos.html]: http://scholarlyediting.org/2014/editions/intro.%20avisos.html

