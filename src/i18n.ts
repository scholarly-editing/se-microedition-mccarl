export interface Ii18n { [key: string]: {
  en: string
  es: string
} }

const i18n: Ii18n = {
  DisplayOptions: {
    en: "Display Options",
    es: "Opciones de representación"
  },
  GeneralOptions: {
    en: "General Options",
    es: "Opciones generales"
  },
  SemanticMarkup: {
    en: "Semantic Markup",
    es: "Marcado semántico"
  },
  Bibliographical: {
    en: "Bibliographical",
    es: "Bibliográfico"
  },
  Contextual: {
    en: "Contextual",
    es: "Contextual"
  },
  Discursive: {
    en: "Discursive",
    es: "Discursivo"
  },
  Annotations: {
    en: "Annotations",
    es: "Anotaciones"
  },
  Structured: {
    en: "Structured",
    es: "Estructuradas"
  },
  Unstructured: {
    en: "Unstructured",
    es: "No estructuradas"
  },
  HierarchicalTip: {
    es: "Introducir sangría para poner en evidencia la relación entre los textos principales y las ediciones y traducciones de ellos (ver introducción, secciones 1.1 y 2.0)",
    en: "Indent entries to show relationship between primary entries and subsequent editions and translations (see introduction, sections 1.1 and 2.0)"
  },
  SemanticMarkupTip: {
    es: "Resaltar el marcado semántico (ver introducción, secciones 1.2 y 2.0)",
    en: "Highlight semantic markup (see introduction, sections 1.2 and 2.0)"
  },
  AnnotationsTip: {
    es: "Mostrar las anotaciones (ver introducción, secciones 1.2 y 2.0)",
    en: "Show annotations (see introduction, sections 1.2 and 2.0)"
  },
  All: {
    en: "All",
    es: "Todo"
  },
  AllFemininePlural: {
    en: "All",
    es: "Todas"
  },
  Doubt: {
    en: "Doubt",
    es: "Duda"
  },
  Criticism: {
    en: "Criticism",
    es: "Crítica"
  },
  Correction: {
    en: "Correction",
    es: "Corrección"
  },
  Praise: {
    en: "Praise",
    es: "Alabanza"
  },
  SelfPromotion: {
    en: "Self-Promotion",
    es: "Auto promoción"
  },
  EyeWitness: {
    en: "Eye Witness",
    es: "Testimonio presencial"
  },
  PublicationStatus: {
    en: "Publication Status",
    es: "Estado de publicación"
  },
  AuthorsSources: {
    en: "Author's Sources",
    es: "Fuentes del autor"
  },
  RegularizedData: {
    en: "Regularized Data",
    es: "Datos regularizados"
  },
  EditorsSources: {
    en: "Editor's Sources",
    es: "Fuentes del editor"
  },
  ModernEditions: {
    en: "Modern Editions",
    es: "Ediciones modernas"
  },
  DigitalVersions: {
    en: "Digital Versions",
    es: "Versiones digitales"
  },
  LexicalNotes: {
    en: "Lexical Notes",
    es: "Notas léxicas"
  },
  ContextualNotes: {
    en: "Contextual Notes",
    es: "Notas contextuales"
  },
  Author: {
    en: "Author/Editor",
    es: "Autor/Editor"
  },
  Title: {
    en: "Title",
    es: "Título"
  },
  PlaceOfPublication: {
    en: "Place of Publication",
    es: "Lugar de publicación"
  },
  Printer: {
    en: "Printer",
    es: "Imprenta"
  },
  Date: {
    en: "Date",
    es: "Fecha"
  },
  Size: {
    en: "Size",
    es: "Tamaño"
  },
  Language: {
    en: "Language",
    es: "Idioma"
  },
  State: {
    en: "State",
    es: "Estado"
  },
  ContextualDate: {
    en: "Contextual Date",
    es: "Fecha contextual"
  },
  Person: {
    en: "Person",
    es: "Persona"
  },
  Place: {
    en: "Place",
    es: "Lugar"
  },
  HierarchicalView: {
    en: "Hierarchical View",
    es: "Vista jerárquica"
  },
  BibliographicalNotes: {
    en: "Bibliograhical Notes",
    es: "Notas bibliográficas"
  }
}

export default i18n