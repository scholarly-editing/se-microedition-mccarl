import React from "react"
import { useStaticQuery, graphql } from "gatsby"
import { IGatsbyImageData } from "gatsby-plugin-image"
import Ceteicean, {Routes} from "gatsby-theme-ceteicean/src/components/Ceteicean"
import {
  TeiHeader
} from "gatsby-theme-ceteicean/src/components/DefaultBehaviors"

import Pb from "./Pb"
import Seg from "./Seg"
import Date from "./Date"
import Bibl from "./Bibl"
import Note from "./Note"
import Ref from "./Ref"

import { ThemeProvider } from "@material-ui/core/styles"
import CssBaseline from "@material-ui/core/CssBaseline"
import Container from "@material-ui/core/Container"
import { makeStyles } from '@material-ui/core/styles'

import theme from "../../theme"

import SEO from "../../components/seo"
import Layout from "../../components/layout"
import MicroEdAppbar from "../../components/microedAppbar"

import useDisplayStyles from "../../displayOptions"
import Bibliographic from "./bibliographic"
import Contextual from "./contextual"

export type Fac = {
  name: string
  childImageSharp: {
    gatsbyImageData: IGatsbyImageData
  }
}

interface Props {
  pageContext: {
    name: string
    prefixed: string
    elements: string[]
  },
  location: string
}

export type Language = "es" | "en"

export type LanguageContextType = {
  language: Language
  setLanguage: React.Dispatch<React.SetStateAction<Language>>
}

export const LanguageContext = React.createContext<LanguageContextType>({
  language: "es",
  setLanguage: () => console.warn('no language provider')
})

export interface IOptions { [key: string]: boolean }

export type ContextType = {
  contextOpts: IOptions
  setContextOpts: React.Dispatch<React.SetStateAction<IOptions>>
}

export const SemanticMarkupContext = React.createContext<ContextType>({
  contextOpts: {},
  setContextOpts: () => console.warn('no SemanticMarkup options provider')
})

export const StructAnnotationsContext = React.createContext<ContextType>({
  contextOpts: {},
  setContextOpts: () => console.warn('no StructAnnotation options provider')
})

export const UnstructAnnotationsContext = React.createContext<ContextType>({
  contextOpts: {},
  setContextOpts: () => console.warn('no UnstructAnnotation options provider')
})

export const TextOptsContext = React.createContext<ContextType>({
  contextOpts: {},
  setContextOpts: () => console.warn('no TextOptsContext options provider')
})

export const BibliographicContext = React.createContext<ContextType>({
  contextOpts: {},
  setContextOpts: () => console.warn('no BibliographicContext options provider')
})

export const ContextualContext = React.createContext<ContextType>({
  contextOpts: {},
  setContextOpts: () => console.warn('no ContextualContext options provider')
})

const useStyles = makeStyles(() => ({
  en: {
    "& tei-tei *[lang=en]": {
      display: "initial"
    },
    "& tei-tei *[lang=es]": {
      display: "none"
    },
  }
}))

export default function MicroEdCeteicean({pageContext}: Props) {
  const queryData = useStaticQuery(graphql`
    query general {
      facs: allFile(filter: {relativeDirectory: {in: "facs"}}) {
        nodes {
          name
          childImageSharp {
            gatsbyImageData
          }
        }
      }
    }
  `)
  const facs: Fac[] = queryData.facs.nodes
  const classes = useStyles()

  const routes: Routes = {
    "tei-teiheader": TeiHeader,
    "tei-ref": Ref,
    "tei-pb": (props) => <Pb facs={facs} {...props}/>,
    "tei-seg": Seg,
    "tei-author": Bibliographic,
    "tei-editor": Bibliographic,
    "tei-title": Bibliographic,
    "tei-pubPlace": Bibliographic,
    "tei-publisher": Bibliographic,
    "tei-date": Date,
    "tei-dim": Bibliographic,
    "tei-textLang": Bibliographic,
    "tei-person": Contextual,
    "tei-place": Contextual,
    "tei-bibl": Bibl,
    "tei-note": Note,
  }

  let styleClasses = ''

  const [language, setLanguage] = React.useState('es' as Language)

  if (language === 'en') {
    styleClasses += classes.en
  }

  const [semanticMarkupOpts, setSemanticMarkupOpts] = React.useState({} as IOptions)
  const [structAnnotationsOpts, setStructAnnotationsOpts] = React.useState({} as IOptions)
  const [unstructAnnotationsOpts, setUnstructAnnotationsOpts] = React.useState({} as IOptions)
  const [textOpts, setTextOpts] = React.useState({} as IOptions)
  const [bibliographicOpts, setBibliographicOpts] = React.useState({} as IOptions)
  const [contextualOpts, setContextualOpts] = React.useState({} as IOptions)

  const displayStyles: { [key: string]: string } = useDisplayStyles()
  
  // TODO: simplify
  for (const k in semanticMarkupOpts) {
    const key = k as keyof IOptions
    if (semanticMarkupOpts[key]) {
      styleClasses += ` ${displayStyles[key]}`
    }
  }
  for (const k in structAnnotationsOpts) {
    const key = k as keyof IOptions
    if (structAnnotationsOpts[key]) {
      styleClasses += ` ${displayStyles[key]}`
    }
  }
  for (const k in unstructAnnotationsOpts) {
    const key = k as keyof IOptions
    if (unstructAnnotationsOpts[key]) {
      styleClasses += ` ${displayStyles[key]}`
    }
  }
  for (const k in textOpts) {
    const key = k as keyof IOptions
    if (textOpts[key]) {
      styleClasses += ` ${displayStyles[key]}`
    }
  }
  for (const k in bibliographicOpts) {
    const key = k as keyof IOptions
    if (bibliographicOpts[key]) {
      styleClasses += ` ${displayStyles[key]}`
    } 
  }
  for (const k in contextualOpts) {
    const key = k as keyof IOptions
    if (contextualOpts[key]) {
      styleClasses += ` ${displayStyles[key]}`
    } 
  }

  return(
    <LanguageContext.Provider value={{language, setLanguage}}>
      <SemanticMarkupContext.Provider value={{
        contextOpts: semanticMarkupOpts,
        setContextOpts: setSemanticMarkupOpts }}>
        <StructAnnotationsContext.Provider value={{
          contextOpts: structAnnotationsOpts,
          setContextOpts: setStructAnnotationsOpts }}>
          <UnstructAnnotationsContext.Provider value={{
            contextOpts: unstructAnnotationsOpts,
            setContextOpts: setUnstructAnnotationsOpts }}>
            <TextOptsContext.Provider value={{
              contextOpts: textOpts,
              setContextOpts: setTextOpts }}>
              <BibliographicContext.Provider value={{
                contextOpts: bibliographicOpts,
                setContextOpts: setBibliographicOpts }}>
                <ContextualContext.Provider value={{
                  contextOpts: contextualOpts,
                  setContextOpts: setContextualOpts }}>
                  <Layout appbar={<MicroEdAppbar/>} location="epítome">
                    <SEO title="Edition" />
                    <ThemeProvider theme={theme}>
                      <CssBaseline />
                      <Container component="main" maxWidth="md" className={styleClasses}>
                        <Ceteicean pageContext={pageContext} routes={routes} />
                      </Container>
                    </ThemeProvider>
                  </Layout>
                </ContextualContext.Provider>
              </BibliographicContext.Provider>
            </TextOptsContext.Provider>
          </UnstructAnnotationsContext.Provider>
        </StructAnnotationsContext.Provider>
      </SemanticMarkupContext.Provider>
    </LanguageContext.Provider>
  )

}
