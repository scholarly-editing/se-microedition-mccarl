import React from "react"
import { TBehavior, SafeUnchangedNode } from "gatsby-theme-ceteicean/src/components/DefaultBehaviors"
import { makeStyles, Tooltip, useMediaQuery } from "@material-ui/core"
import { Behavior } from "gatsby-theme-ceteicean/src/components/Behavior"
import { ContextualContext, LanguageContext } from "./Ceteicean"
import i18n from '../../i18n'
import theme from "../../theme"

import Bibliographic from "./bibliographic"

interface TEIProps {
  teiNode: Node,
  availableRoutes?: string[]
}

const useStyles = makeStyles(() => ({
  tooltip: {
    color: '#fff',
    fontSize: '12pt',
    border: "2px solid #fff"
  }
}))

const Date: TBehavior = (props: TEIProps) => {
  const { contextOpts } = React.useContext(ContextualContext)
  const { language } = React.useContext(LanguageContext)
  const [open, setOpen] = React.useState(false)

  const isScreenSmall = useMediaQuery(theme.breakpoints.down('md'))
  const ttProps: {open?: boolean} = {}

  if (isScreenSmall) {
    ttProps.open = open
  }

  const el = props.teiNode as Element

  const type = el.getAttribute('type')
  const classes = useStyles()

  if (type === "contextual" && contextOpts.hasOwnProperty("ContextualDate")) {
    const title = i18n.ContextualDate[language]
    return (
      <Behavior node={props.teiNode}>
        <Tooltip title={title} placement="top" {...ttProps} onClose={() => setOpen(false)}
          classes={{tooltip: classes.tooltip}}>
          <span onClick={() => setOpen(true)}>
            <SafeUnchangedNode {...props} />
          </span>
        </Tooltip>
      </Behavior>
    )
  }

  return <Bibliographic {...props}/>
}

export default Date
