import React from "react"
import { TBehavior, SafeUnchangedNode } from "gatsby-theme-ceteicean/src/components/DefaultBehaviors"
import { makeStyles, Tooltip, useMediaQuery } from "@material-ui/core"
import { Behavior } from "gatsby-theme-ceteicean/src/components/Behavior"
import { StructAnnotationsContext, LanguageContext } from "./Ceteicean"
import i18n from '../../i18n'
import theme from "../../theme"

import { Colors, IColors } from '../../displayOptions'

interface TEIProps {
  teiNode: Node,
  availableRoutes?: string[]
}

const subTypeMap: {[key: string]: string} = {
  "author_source": "AuthorsSources",
  "regularized": "RegularizedData",
  "editor_source": "EditorsSources",
  "modern_version": "ModernEditions",
  "digital_version": "DigitalVersions",
}

const useStyles = makeStyles(() => ({
  tooltip: {
    backgroundColor: (props: {color: string}) => props.color,
    color: '#000',
    fontSize: '12pt',
    border: "2px solid #fff"
  }
}))

const Bibl: TBehavior = (props: TEIProps) => {

  const {contextOpts} = React.useContext(StructAnnotationsContext)
  const { language } = React.useContext(LanguageContext)
  const [open, setOpen] = React.useState(false)

  const isScreenSmall = useMediaQuery(theme.breakpoints.down('md'))
  const ttProps: {open?: boolean} = {}

  if (isScreenSmall) {
    ttProps.open = open
  }

  const el = props.teiNode as Element

  const subType = el.getAttribute('subtype')
  const color = subType ? Colors[subTypeMap[subType] as keyof IColors] || "#707070" : "#707070"
  const classes = useStyles({color})

  if (subType) {
    if (contextOpts.hasOwnProperty(subTypeMap[subType])) {
      const title = i18n[subTypeMap[subType]][language]
      return (
        <Behavior node={props.teiNode}>
          <Tooltip title={title} placement="top" {...ttProps} onClose={() => setOpen(false)}
            classes={{tooltip: classes.tooltip}}>
            <span onClick={() => setOpen(true)}>
              <SafeUnchangedNode {...props} />
            </span>
          </Tooltip>
        </Behavior>
      )
    }
  }
  return <SafeUnchangedNode {...props} />
}

export default Bibl
