import React from "react"
import { TBehavior } from "gatsby-theme-ceteicean/src/components/DefaultBehaviors"
import { Behavior } from "gatsby-theme-ceteicean/src/components/Behavior"
import { TEINodes } from "react-teirouter"

import { BibliographicContext, IOptions } from "./Ceteicean"

interface TEIProps {
  teiNode: Node,
  availableRoutes?: string[]
}

export const Ref: TBehavior = (props: TEIProps) => {
  const { contextOpts, setContextOpts } = React.useContext(BibliographicContext)

  const el = props.teiNode as Element
  const target = el.getAttribute("target") || ""

  const makeVisible = () => {
    if (target.startsWith('#')) {
      const targetEl = el.ownerDocument.getElementById(target.replace('#', ''))
      if (targetEl) {
        if (targetEl.tagName.toLowerCase() === 'tei-bibl' && targetEl.getAttribute('type') === 'orig') {
          const newOpts: IOptions = Object.assign({}, contextOpts)
          newOpts.BibliographicalNotes = true
          setContextOpts(newOpts)
        }
        // Special case for seemingly orphaned elements
        if (!targetEl.offsetParent) {
          let newTargetEl = document.querySelector(`tei-note ${target}`)
          if (!newTargetEl) {
            newTargetEl = document.querySelector(`${target}`)?.parentElement?.parentElement || null
          }
          newTargetEl?.scrollIntoView()
        }
      }
    }
  }
  return (<Behavior node={props.teiNode}>
    <a href={target} onClick={makeVisible}>
      {<TEINodes 
          teiNodes={el.childNodes}
          {...props}/>}
    </a>
  </Behavior>)
}

export default Ref
