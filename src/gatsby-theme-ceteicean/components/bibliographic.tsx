import React from "react"
import { TBehavior, SafeUnchangedNode } from "gatsby-theme-ceteicean/src/components/DefaultBehaviors"
import { makeStyles, Tooltip, useMediaQuery } from "@material-ui/core"
import { Behavior } from "gatsby-theme-ceteicean/src/components/Behavior"
import { BibliographicContext, LanguageContext } from "./Ceteicean"
import i18n from '../../i18n'
import theme from "../../theme"

import { Colors, IColors } from '../../displayOptions'

interface TEIProps {
  teiNode: Node,
  availableRoutes?: string[]
}

const teiMap: {[key: string]: string} = {
  "tei-author": "Author",
  "tei-editor": "Author",
  "tei-title": "Title",
  "tei-pubPlace": "PlaceOfPublication",
  "tei-publisher": "Printer",
  "tei-date": "Date",
  "tei-dim": "Size",
  "tei-textLang": "Language"
}

const useStyles = makeStyles(() => ({
  tooltip: {
    backgroundColor: (props: {color: string}) => props.color,
    color: '#000',
    fontSize: '12pt',
    border: "2px solid #fff"
  }
}))

const findAncestor = (el: Element | null | undefined) => {
  while (el) {
    if (el.tagName === 'tei-bibl' && el.getAttribute("type") === "orig") {
      return true
    }
    el = el.parentElement
  }
  return false
}

const Bibliographic: TBehavior = (props: TEIProps) => {

  const { contextOpts } = React.useContext(BibliographicContext)
  const { language } = React.useContext(LanguageContext)
  const [open, setOpen] = React.useState(false)

  const isScreenSmall = useMediaQuery(theme.breakpoints.down('md'))
  const ttProps: {open?: boolean} = {}

  if (isScreenSmall) {
    ttProps.open = open
  }

  const el = props.teiNode as Element
  const tagName = el.tagName.toLowerCase()

  const isBiblio = findAncestor(el)
  const color = isBiblio ? Colors[teiMap[tagName] as keyof IColors] || "#707070" : "#707070"
  const classes = useStyles({color})

  if (isBiblio) {
    if (contextOpts.hasOwnProperty(teiMap[tagName])) {
      const title = i18n[teiMap[tagName]][language]
      return (
        <Behavior node={props.teiNode}>
          <Tooltip title={title} placement="top" {...ttProps} onClose={() => setOpen(false)}
            classes={{tooltip: classes.tooltip}}>
            <span onClick={() => setOpen(true)}>
              <SafeUnchangedNode {...props} />
            </span>
          </Tooltip>
        </Behavior>
      )
    }
  }
  return <SafeUnchangedNode {...props} />
}

export default Bibliographic
