import React from "react"
import { Dialog, DialogContent, DialogContentText, DialogTitle, IconButton, List,
  ListItem, ListItemIcon, ListItemSecondaryAction, ListItemText, makeStyles, Typography } from "@material-ui/core"
import { Close, Code, FolderOpen, GetApp } from "@material-ui/icons"
import { navigate } from 'gatsby'

interface Props {
  open: boolean
  close: () => void
}

const Download = ({open, close}: Props) => {

  const useStyles = makeStyles(() => ({
    dialogTitle: {
      display: "flex",
      justifyContent: "space-between",
      alignItems: "center"
    },
    dialogImage: {
      marginBottom: "1rem",
      textAlign: "center"
    },
    dialogItem: {
      "& span": {
        fontSize: "1rem !important"
      }
    }
  }))
  const classes = useStyles()  

  return (<Dialog
    open={open}
    scroll="body"
    onClose={close}
    aria-labelledby="alert-dialog-slide-title"
    aria-describedby="alert-dialog-slide-description"
  >
    <DialogTitle id="alert-dialog-slide-title" disableTypography className={classes.dialogTitle}>
      <Typography variant="h6">Download TEI data</Typography>
      <IconButton aria-label="close download dialog" onClick={close}>
        <Close />
      </IconButton>
    </DialogTitle>
    <DialogContent>      
      <DialogContentText component="div" id="alert-dialog-slide-description">
       <p>This edition is encoded according to the <a href="https://tei-c.org/">Text Encoding Initiative</a> (TEI) P5 Guidelines.</p>
       <p>The open source code for this edition is avilable on <a href="https://gitlab.com/scholarly-editing/se-microedition-mccarl/">
            GitLab</a>.</p>
       <List dense>
        <ListItem className={classes.dialogItem}>
          <ListItemIcon>
            <Code />
          </ListItemIcon>
          <ListItemText primary="TEI XML data" />
          <ListItemSecondaryAction>
            <IconButton edge="end" aria-label="download" onClick={() => navigate('/tei/epítome.xml')}>
              <GetApp />
            </IconButton>
          </ListItemSecondaryAction>
        </ListItem>
        <ListItem className={classes.dialogItem}>
          <ListItemIcon>
            <Code />
          </ListItemIcon>
          <ListItemText primary="Customization ODD" />
          <ListItemSecondaryAction>
            <IconButton edge="end" aria-label="download" onClick={() => navigate('/epítome.odd')}>
              <GetApp />
            </IconButton>
          </ListItemSecondaryAction>
        </ListItem>
        <ListItem className={classes.dialogItem}>
          <ListItemIcon>
            <Code />
          </ListItemIcon>
          <ListItemText primary="RelaxNG schema" />
          <ListItemSecondaryAction>
            <IconButton edge="end" aria-label="download" onClick={() => navigate('/epítome.rng')}>
              <GetApp />
            </IconButton>
          </ListItemSecondaryAction>
        </ListItem>
        <ListItem className={classes.dialogItem}>
          <ListItemIcon>
            <FolderOpen />
          </ListItemIcon>
          <ListItemText primary="Download all (zipped)" />
          <ListItemSecondaryAction>
            <IconButton edge="end" aria-label="download" onClick={() => navigate('/se-microedition-mccarl.zip')}>
              <GetApp />
            </IconButton>
          </ListItemSecondaryAction>
        </ListItem>
      </List>
      </DialogContentText>
      
    </DialogContent>
  </Dialog>)
}

export default Download