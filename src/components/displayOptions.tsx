import React from "react"

import MenuItem from '@material-ui/core/MenuItem'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import Switch from '@material-ui/core/Switch'
import ListItem from "@material-ui/core/ListItem"
import { withStyles, makeStyles } from '@material-ui/core/styles'

import { Colors, IColors } from '../displayOptions'
import { ContextType, IOptions, Language } from '../gatsby-theme-ceteicean/components/Ceteicean'

import theme from '../theme'
import i18n, { Ii18n } from '../i18n'
import Backdrop from "@material-ui/core/Backdrop"
import { Typography } from "@material-ui/core"

const OptionSwitch = withStyles({
  switchBase: (props: {customColor: string}) => ({
    '&.Mui-checked': {
      color: props.customColor,
    },
    "&.Mui-checked + .MuiSwitch-track": {
      backgroundColor: props.customColor,
    }
  }),
})(Switch)

interface Props {
  context: ContextType
  options: string[]
  customColors?: boolean
  all?: boolean
  genderedAll?: "f" | "m" | "o"
  lang?: Language
  component?: "MenuItem" | "ListItem"
}

const useStyles = makeStyles(() => ({
  label: {
    paddingBottom: 0
  },
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: '#000',
  },
  mark: {
    backgroundColor: "#fff",
    padding: "10px"
  }
}))

export default function DisplayOptions({context, options, customColors = false, all = false, component = 'MenuItem', lang = "en", genderedAll}: Props) {  
  const classes = useStyles()
  const { contextOpts, setContextOpts } = context

  const [spinner, setSpinner] = React.useState(false)

  // All switch  
  const [allChecked, setAllChecked] = React.useState(Object.keys(contextOpts).length === options.length)

  const toggleOpt = (key: keyof IOptions) => {
    const newOpts: IOptions = Object.assign({}, contextOpts)
    if (key in contextOpts) {
      delete newOpts[key]
      setAllChecked(false)
    } else {
      newOpts[key] = true
    }
    setContextOpts(newOpts)
  }

  const toggleAllOpts = () => {
    const newOpts: IOptions = {}
    if (!allChecked) {
      for (const k of options) {
        newOpts[k] = true
      }
    }
    setContextOpts(newOpts)
    setAllChecked(!allChecked)
  }

  const spinAndUpdate = React.useCallback((update) => {
    setSpinner(true)
    // this makes sure the spinner is displayed before actually updating the options.
    setTimeout(update, 1)
  }, [spinner])

  let allSwitch: JSX.Element | null = null

  const MenuItemWrapper = ({children}: {children: any}) => (
    <MenuItem
      onKeyUp={(e: React.KeyboardEvent) => {
        if (e.key === " ") {
          spinAndUpdate(toggleAllOpts)
        }
      }}>
      {children}
    </MenuItem>
  )

  const ListItemWrapper = ({children}: {children: any}) => (
    <ListItem
      onKeyUp={(e: React.KeyboardEvent) => {
        if (e.key === " ") {
          spinAndUpdate(toggleAllOpts)
        }
      }}>
      {children}
    </ListItem>
  )

  const Wrapper = 
    component === 'MenuItem' ? MenuItemWrapper : 
    component === 'ListItem' ? ListItemWrapper :
                               MenuItemWrapper
  
  if (all) {
    allSwitch = (<Wrapper>
      <Backdrop className={classes.backdrop} open={spinner}>
        <Typography variant="h4" className={classes.backdrop}>
          <mark className={classes.mark}>Applying changes...</mark>
        </Typography>
      </Backdrop>
      <FormControlLabel 
        control={
          <OptionSwitch
            customColor={theme.palette.primary.main}
            name="All"
            checked={allChecked}
            onClick={() => {
              spinAndUpdate(toggleAllOpts)
            }}
          />
        }
        label={genderedAll === 'f' ? i18n.AllFemininePlural[lang] : i18n.All[lang]}
        classes={classes}
      />
    </Wrapper>)
  }

  React.useLayoutEffect(() => {
    setSpinner(false)
  }, [contextOpts])

  /* FIXME: when updating to M-UI 5.0 we should be able to use ref normally */
  return (<>
    {allSwitch}
    {options.map((item: string) => {
      let color = theme.palette.primary.main
      if (customColors) {
        color = Colors[item as keyof IColors]
      }
      return (<Wrapper>
        <FormControlLabel
          control={
            <OptionSwitch
              customColor={color ? color : theme.palette.primary.main}
              name={item}
              checked={contextOpts[item] || false}
              onClick={() => toggleOpt(item)}
            />
          }
          label={i18n[item as keyof Ii18n][lang]}
          classes={classes}
        />
      </Wrapper>
    )})}
  </>)

}