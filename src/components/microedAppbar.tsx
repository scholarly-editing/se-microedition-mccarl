import React from "react"

import Container from "@material-ui/core/Container"
import AppBar from "@material-ui/core/AppBar"
import Toolbar from "@material-ui/core/Toolbar"
import useScrollTrigger from '@material-ui/core/useScrollTrigger'
import Typography from '@material-ui/core/Typography'
import MenuItem from '@material-ui/core/MenuItem'
import useMediaQuery from '@material-ui/core/useMediaQuery'

import { makeStyles } from '@material-ui/core/styles'

import {
  SemanticMarkupContext,
  StructAnnotationsContext,
  UnstructAnnotationsContext,
  TextOptsContext,
  BibliographicContext,
  ContextualContext,
  LanguageContext
} from '../gatsby-theme-ceteicean/components/Ceteicean'

import DisplayOptionsMenu from './displayOptionsMenu'
import DisplayOptions from "./displayOptions"
import Button from "@material-ui/core/Button"
import Dialog from "@material-ui/core/Dialog"
import { TransitionProps } from "@material-ui/core/transitions/transition"
import Slide from "@material-ui/core/Slide"
import { DialogContent, DialogContentText, DialogTitle, IconButton, List } from "@material-ui/core"
import { Close, Menu } from "@material-ui/icons"

import i18n from '../i18n'

import theme from "../theme"

const useStyles = makeStyles(() => ({
  appbarContent: {
    display: 'flex'
  },
  appbarTop: {
    marginTop: '-1.5rem',
    marginBottom: '2.5em'
  },
  appbarSpacer: {
    flex: '1 1 auto'
  },
  menuGutter: {
    padding: '6px 16px'
  },
  dialogTitle: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center"
  },
  optionsArea: {
    borderRadius: "10px",
    borderStyle: "solid",
    borderColor: "rgba(0, 0, 0, 0.12)",
    borderWidth: "1px"
  },
  optionsAreaLarge: {
    padding: "24px"
  },
  optionsRow: {
    display: "flex",
    flexDirection: "row"
  },
  optionsColumn: {
    display: "flex",
    flexDirection: "column",
    flexBasis: "100%",
    flex: 1
  },
  gloss: {
    margin: "0 0 16px 16px",
    fontStyle: "italic",
  }
}))

function FixedScroll({children}: {children: JSX.Element}) {
  const classes = useStyles()  
  const trigger = useScrollTrigger({
    disableHysteresis: true,
    threshold: 250,
  })

  return React.cloneElement(children, {
    position: trigger ? 'fixed' : 'static',
    className: trigger ? '' : classes.appbarTop
  })
}

const Transition = React.forwardRef(function Transition(
  props: TransitionProps & { children?: React.ReactElement<any, any> },
  ref: React.Ref<unknown>,
) {
  return <Slide direction="up" ref={ref} {...props} />
})

export default function MicroedAppBar() {
  const classes = useStyles()
  const [dialogVisibility, setDialogVisibility] = React.useState<boolean>(false)

  const {language, setLanguage} = React.useContext(LanguageContext)
  const languages = {
    "es": "Español",
    "en": "English"
  } 

  const semanticMarkupOpts = React.useContext(SemanticMarkupContext)
  const structAnnotationsOpts = React.useContext(StructAnnotationsContext)
  const unstructAnnotationsOpts = React.useContext(UnstructAnnotationsContext)
  const textOpts = React.useContext(TextOptsContext)
  const bibliographicOpts = React.useContext(BibliographicContext)
  const contextualOpts = React.useContext(ContextualContext)

  const isScreenSmall = useMediaQuery(theme.breakpoints.down('sm'))
  const optionsColumnClass = isScreenSmall ? '' : classes.optionsColumn
  const optionsPadding = isScreenSmall ? '' : classes.optionsAreaLarge
  const optionsRow = isScreenSmall ? '' : classes.optionsRow
  const dialogWidth = isScreenSmall ? 'sm' : 'md'

  const handleDialogOpen = () => {
    setDialogVisibility(!dialogVisibility)
  }

  return (<>
    <FixedScroll>
      <AppBar>
        <Toolbar>
          <Container maxWidth={dialogWidth} className={classes.appbarContent}>
            <DisplayOptionsMenu label={languages[language]} closeOnClick>
              <>
                <MenuItem onClick={() => {setLanguage('es')}}>Español</MenuItem>
                <MenuItem onClick={() => {setLanguage('en')}}>English</MenuItem>
              </>
            </DisplayOptionsMenu>
            <div className={classes.appbarSpacer}/>
            <Button 
              color="secondary"
              endIcon={<Menu/>}
              onClick={handleDialogOpen}>
              {i18n.DisplayOptions[language]}
            </Button>
          </Container>
        </Toolbar>
      </AppBar>
    </FixedScroll>
    <Dialog
      maxWidth="md"
      fullWidth={true}
      open={dialogVisibility}
      scroll="body"
      TransitionComponent={Transition}
      keepMounted
      onClose={handleDialogOpen}
      aria-labelledby="alert-dialog-slide-title"
    >
      <DialogTitle id="alert-dialog-slide-title" disableTypography className={classes.dialogTitle}>
        <Typography variant="h6">{i18n.DisplayOptions[language]}</Typography>
        <IconButton aria-label="close display options" onClick={handleDialogOpen}>
          <Close />
        </IconButton>
      </DialogTitle>
      <DialogContent>        
        <DialogContentText component="div">
          <div className={`${classes.optionsArea} ${optionsPadding}`}>
            <Typography className={classes.menuGutter} variant="overline">{i18n.GeneralOptions[language]}</Typography>
            <Typography className={classes.gloss} variant="body2" component="div">{i18n.HierarchicalTip[language]}</Typography>
            <div className={optionsRow}>
              <List className={optionsColumnClass}>
                <DisplayOptions
                  context={textOpts}
                  component="ListItem"
                  lang={language}
                  options={['HierarchicalView']} />
              </List>
            </div>
          </div>
          <div className={`${classes.optionsArea} ${optionsPadding}`}>
            <Typography component="div" className={classes.menuGutter} variant="overline">{i18n.SemanticMarkup[language]}</Typography>
            <Typography className={classes.gloss} variant="body2">{i18n.SemanticMarkupTip[language]}</Typography>
            <div className={optionsRow}>
              <List subheader={<Typography className={classes.menuGutter} variant="caption">{i18n.Bibliographical[language]}</Typography>} className={optionsColumnClass}>
                <DisplayOptions
                  context={bibliographicOpts}
                  component="ListItem"
                  customColors
                  all
                  lang={language}
                  options={['Author', 'Title', 'PlaceOfPublication', 'Printer', 'Date', 'Size', 'Language', 'State']} />
              </List>
              
              <List subheader={<Typography className={classes.menuGutter} variant="caption">{i18n.Discursive[language]}</Typography>} className={optionsColumnClass}>
                <DisplayOptions
                  component="ListItem"
                  context={semanticMarkupOpts}
                  customColors
                  all
                  lang={language}
                  options={['Doubt', 'Criticism', 'Correction', 'Praise', 'SelfPromotion', 'EyeWitness', 'PublicationStatus']} />
              </List>

              <List subheader={<Typography className={classes.menuGutter} variant="caption">{i18n.Contextual[language]}</Typography>} className={optionsColumnClass}>
                <DisplayOptions
                  component="ListItem"
                  context={contextualOpts}
                  customColors
                  all
                  lang={language}
                  options={['ContextualDate', 'Person', 'Place']} />
              </List>
            </div>
          </div>
          <div className={`${classes.optionsArea} ${optionsPadding}`}>
            <Typography component="div" className={classes.menuGutter} variant="overline">{i18n.Annotations[language]}</Typography>
            <Typography className={classes.gloss} variant="body2">{i18n.AnnotationsTip[language]}</Typography>
            <div className={optionsRow}>
              <List subheader={<Typography className={classes.menuGutter} variant="caption">{i18n.Structured[language]}</Typography>} className={optionsColumnClass}>
                <DisplayOptions
                  context={structAnnotationsOpts}
                  component="ListItem"
                  customColors
                  all
                  genderedAll="f"
                  lang={language}
                  options={['AuthorsSources', 'RegularizedData', 'EditorsSources', 'ModernEditions', 'DigitalVersions']} />
              </List>

              <List subheader={<Typography className={classes.menuGutter} variant="caption">{i18n.Unstructured[language]}</Typography>} className={optionsColumnClass}>
                <DisplayOptions
                  context={unstructAnnotationsOpts}
                  component="ListItem"  
                  customColors
                  all
                  genderedAll="f"
                  lang={language}
                  options={['LexicalNotes', 'ContextualNotes', 'BibliographicalNotes']} />
              </List>
            </div>
          </div>
        </DialogContentText>
      </DialogContent>
    </Dialog>
  </>)
}