import React from "react"
import { useStaticQuery, graphql } from "gatsby"
import CssBaseline from "@material-ui/core/CssBaseline"
import { ThemeProvider, makeStyles } from "@material-ui/core/styles"
import Container from "@material-ui/core/Container"

import theme from "../theme"
import Header from "./header"
import Footer from "./footer"
import EditionFooter from "./editionFooter"

// Style

const useStyles = makeStyles({
  main: {
    paddingBottom: "1.45rem",
    "& h2, & h3": {
      paddingBottom: "1rem",
    },
  }
})

interface Props {
  location?: string
  appbar?: JSX.Element
  children: JSX.Element | JSX.Element[]
}

const Layout = ({ location, appbar, children }: Props) => {
  const data = useStaticQuery(graphql`
    query SiteTitleQuery {
      site {
        siteMetadata {
          title
          doi
          menuLinks {
            name
            link
          }
        }
      }
    }
  `)

  const classes = useStyles()
  
  const footer = location === "epítome" ? <EditionFooter/> : <Footer/>

  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <Header
        location={location || ''}
        siteTitle={data.site.siteMetadata.title}
        menuLinks={data.site.siteMetadata.menuLinks}
        doi={data.site.siteMetadata.doi}
      />
      {appbar}
      <Container component="main" maxWidth="md" className={classes.main}>
        {children}
      </Container>
      {footer}
    </ThemeProvider>
  )
}

export default Layout
