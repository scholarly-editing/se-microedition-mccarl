import React from "react"

import Button from "@material-ui/core/Button"
import ClickAwayListener from '@material-ui/core/ClickAwayListener'
import Popper from '@material-ui/core/Popper'
import Grow from '@material-ui/core/Grow'
import Paper from '@material-ui/core/Paper'
import MenuList from '@material-ui/core/MenuList'

import ExpandMore from '@material-ui/icons/ExpandMore'

interface Props {
  children?: JSX.Element
  label: string
  closeOnClick?: boolean
}

export default function DisplayOptionsMenu({children, label, closeOnClick = false}: Props) {  
  // Menu Visibility
  const [open, setOpen] = React.useState(false)
  const anchorRef = React.useRef<HTMLAnchorElement>(null)

  const handleToggle = () => {
    setOpen((prevOpen) => !prevOpen)
  }

  const handleClose = (event: React.MouseEvent<Document, MouseEvent> | React.MouseEvent) => {
    const el = event.target
    const node = anchorRef.current
    if (el instanceof Node && node && node.contains(el)) {
      return
    }

    setOpen(false)
  }

  const handleListKeyDown = (event: React.KeyboardEvent) => {
    if (event.key === 'Tab') {
      event.preventDefault()
      setOpen(false)
    }
  }

  const handleClick = () => {
    if (closeOnClick) {
      setOpen(false)
    }
  }

  // return focus to the button when we transition from closed to open
  const prevOpen = React.useRef(open);
  React.useEffect(() => {
    if (prevOpen.current === true && open === false) {
      anchorRef?.current?.focus()
    }

    prevOpen.current = open
  }, [open])

  /* FIXME: when updating to M-UI 5.0 we should be able to use ref normally */
  return (<>
    <Button 
      {...{ ref: anchorRef } as any}
      color="secondary"
      endIcon={<ExpandMore/>}
      aria-controls={open ? 'menu-list-grow' : undefined}
      aria-haspopup="true"
      onClick={handleToggle}>
      {label}
    </Button>
    <Popper style={{zIndex: 99}} open={open} anchorEl={anchorRef.current} role={undefined} transition disablePortal>
      {({ TransitionProps, placement }) => (
        <Grow
          {...TransitionProps}
          style={{ transformOrigin: placement === 'bottom' ? 'center top' : 'center bottom' }}
        >
          <Paper>
            <ClickAwayListener onClickAway={handleClose}>
              <MenuList autoFocusItem={open} id="menu-list-grow" onKeyDown={handleListKeyDown} onClick={handleClick}>
                {children}
              </MenuList>
            </ClickAwayListener>
          </Paper>
        </Grow>
      )}
    </Popper>
  </>)

}