import React from "react"
import { makeStyles } from "@material-ui/core/styles"
import Grid from "@material-ui/core/Grid"
import Container from "@material-ui/core/Container"
import Button from "@material-ui/core/Button"
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft'

import theme from "../theme"

// Style

const useStyles = makeStyles({
  navBtn: {
    borderRadius: 0,
    boxShadow: "none",
    "&:hover, &:focus": {
      backgroundColor: "transparent",
      textDecorationLine: "underline",
      textDecorationColor: theme.palette.primary.main,
      textDecorationThickness: "3px",
      textUnderlineOffset: "2px"
    },
  },
  info: {
    fontSize: "0.875rem",
    lineHeight: "1.75",
    borderRadius: "4px",
    textTransform: "uppercase",
    paddingTop: "10px !important",
    textAlign: "right",
    "& a": {
      color: theme.palette.text.primary
    }
  },
  bkg: {
    backgroundColor: '#fff'
  }
})

// Component

const Info = ({doi}: {doi: string}) => {
  const classes = useStyles()

  return (
    <div className={classes.bkg}>
      <Container maxWidth="md">
        <Grid container={true}>
          <Grid item={true} xs={4}>
            <Button
              size="large"
              className={classes.navBtn}
              onClick={() => location.href='/issues/39'}
            >
              <ChevronLeftIcon/> Vol. 39
            </Button>
          </Grid>
          <Grid item={true} xs={8} className={classes.info}>
            Micro-Edition | DOI: <a href={`https://doi.org/${doi}`}>{doi}</a>
          </Grid>
        </Grid>
      </Container>
    </div>
  )
}

export default Info
