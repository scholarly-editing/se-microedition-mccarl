import { red } from "@material-ui/core/colors"
import { createMuiTheme } from "@material-ui/core/styles"

const mainColor = "#dc3522"

// A custom theme for CETEIcean
// It is not intended to be comprehensive. The rules here are largely derived from earlier work on TEI Boilerplate  
const theme = createMuiTheme({
  typography: {
    fontFamily: "EB Garamond, Serif",
    body1: {
      fontSize: "1.25rem",
      paddingBottom: "1.25rem",
    },
    subtitle1: {
      fontSize: "1.4rem",
    }
  },
  overrides: {
    MuiCssBaseline: {
      "@global": {
        "html": {
          scrollPaddingTop: "70px"
        },
        "@font-face": [
          {
            fontFamily: "EB Garamond",
            fontStyle: "normal",
            fontDisplay: "swap",
            fontWeight: 400,
          },
        ],
        "a, a:visited, a:hover, a:active": {
          color: mainColor,
        },
        body: {
          color: "#444",
        },
        "h1, h2, h3, h4, h5, h6": {
          color: "#333",
        },
        "tei-choice tei-abbr + tei-expan:before, tei-choice tei-expan + tei-abbr:before, tei-choice tei-sic + tei-corr:before, tei-choice tei-corr + tei-sic:before, tei-choice tei-orig + tei-reg:before, tei-choice tei-reg + tei-orig:before": {
          content: `" ("`
        },
        "tei-choice tei-abbr + tei-expan:after, tei-choice tei-expan + tei-abbr:after, tei-choice tei-sic + tei-corr:after, tei-choice tei-corr + tei-sic:after, tei-choice tei-orig + tei-reg:after, tei-choice tei-reg + tei-orig:after": {
          content: `")"`
        },
        "tei-ab": {
          display: "block",
          marginTop: "1em",
          marginBottom: "1em",
        },
        "tei-emph": {
          fontStyle: "italic"
        },
        //main title
        "tei-body > tei-head": {
          fontSize: "220%",
          textIndent: "-0.5em",
          fontWeight: "bold",
        },
        //bibliotecas
        "tei-listBibl > tei-listBibl > tei-head": {
          display: "block",
          fontWeight: "bold",
          fontSize: "180%",
          lineHeight: "250%",
          textAlign: "center",
          
        },
        "tei-head tei-note": {
          fontSize: "1.25rem",
          fontWeight: "normal",
          textAlign: "left",
          lineHeight: "1.5em",
        },
        //títulos
        "tei-listBibl > tei-listBibl > tei-listBibl > tei-head": {
          display: "block",
          lineHeight: "200%",
          fontSize: "130%",
          fontWeight: "bold",
          textAlign: "center",
        },
        //primary entries 
        "tei-bibl[subtype=primary]": {
          display: "block",
          marginTop: "1em",
          marginBottom: "1em",
        },
        //<person> when a container needs to behave like primary entries
        "tei-person[type=entry]": {
          display: "block",
          marginTop: "1em",
          marginBottom: "1em",
        },
        "tei-lb:after": {
          content: "'\\a'",
          whiteSpace: "pre"
        },
        "tei-p": {
          display: "block",
          marginTop: "1em",
          marginBottom: "1em",
          textAlign: "justify"
        },
        "tei-q:before": {
          content: `"“"`
        },
        "tei-q:after": {
          content: `"”."`
        },
        "tei-bibl[subtype=author_source]": {
          display: "none"
        },
        "tei-bibl[subtype=regularized]": {
          display: "none"
        },
        "tei-bibl[subtype=editor_source]": {
          display: "none"
        },
        "tei-bibl[subtype=emodern_version]": {
          display: "none"
        },
        "tei-bibl[subtype=digital_version]": {
          display: "none"
        },
        "tei-note[type=lexical]": {
          display: "none"
        },
        "tei-note[type=contextual]": {
          display: "none"
        },
        //CM adding 
        "tei-note[type=bibliographical]": {
          display: "none"
        },
        "tei-tei *[lang=en]": {
          display: "none"
        },
        "tei-seg[type=proseEllipsis]": {
          display: "block",
          lineHeight: "200%",
          textAlign: "center",
        },
        "tei-bibl[type=ellipsis]": {
          display: "block",
          lineHeight: "200%",
          textAlign: "center",
        },
        "tei-foreign": {
          fontStyle: "italic"
        },
        "tei-mentioned": {
          fontStyle: "italic"
        },
        "tei-title[level=m]": {
          fontStyle: "italic"
        },

        //for formatting all added <bibl>
        "tei-bibl[type=add] > tei-title[level=m]:after": {
          content: `", "`
        },
        //CM adding 20220228 
        "tei-bibl[type=add] > tei-title[level=a]:before": {
          content: `"“"`
        },
        //for level="a", need to add closing quotation marks along with comma
        "tei-bibl[type=add] > tei-title[level=a]:after": {
          content: `",” "`
        },
        "tei-bibl[type=add] > tei-author:after": {
          content: `", "`
        },
        "tei-bibl[type=add] > tei-pubPlace:after": {
          content: `", "`
        },
        "tei-bibl[type=add] > tei-publisher:after": {
          content: `", "`
        },
        "tei-bibl[type=add] > tei-date:after": {
          content: `". "`
        },
        "tei-bibl[type=add] > tei-citedRange:after": {
          content: `". "`
        },
        //this one not working?
        "tei-bibl[type=add] > tei-ref:after": {
          content: `". "`
        },
        "tei-bibl[subtype=editor_source] > tei-editor:after": {
          content: `", ed., "`
        },
        "tei-bibl[subtype=modern_version] > tei-editor:after": {
          content: `", ed., "`
        },
        
        "tei-bibl[subtype=regularized] > tei-editor:after": {
          content: `", [ed.], "`
        },
        "tei-row": {
          display: "block",
        },             

      },
    },
  },
  palette: {
    primary: {
      main: mainColor,
    },
    secondary: {
      main: "#fbebda",
    },
    error: {
      main: red.A400,
    },
    background: {
      default: "#fff",
    },
  },
})

export default theme
