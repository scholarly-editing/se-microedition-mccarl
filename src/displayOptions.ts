import { makeStyles } from '@material-ui/core/styles'

export interface IColors { [key: string]: string }

export const Colors: IColors = {
  Doubt: "rgb(119, 170, 221)",
  Criticism: "rgb(129, 184, 168)",
  Correction: "rgb(238, 136, 102)",
  Praise: "rgb(221, 221, 221)",
  SelfPromotion: "rgb(153, 221, 255)",
  EyeWitness: "rgb(255, 170, 187)",
  PublicationStatus: "rgb(238, 221, 136)",
  AuthorsSources: "rgb(119, 170, 221)",
  RegularizedData: "rgb(129, 184, 168)",
  EditorsSources: "rgb(238, 221, 136)",
  ModernEditions: "rgb(221, 221, 221)",
  DigitalVersions: "rgb(153, 221, 255)",
  LexicalNotes: "rgb(227, 188, 125)",
  ContextualNotes: "rgb(188, 227, 125)",
  BibliographicalNotes: "rgb(255, 170, 187)",
  Author: "rgb(119, 170, 221)",
  Title: "rgb(129, 184, 168)",
  PlaceOfPublication: "rgb(238, 136, 102)",
  Printer: "rgb(221, 221, 221)",
  Date: "rgb(153, 221, 255)",
  Size: "rgb(255, 170, 187)",
  Language: "rgb(170, 68, 153)",
  State: "rgb(188, 227, 125)",
}

const hierarchicalStyle = {
  "& tei-bibl[subtype=other_edition]": {
    display: "block",
    paddingTop: "25px",
    marginLeft: "25px"
  },
  "& tei-bibl[subtype=translation]": {
    display: "block",
    paddingTop: "25px",
    marginLeft: "25px"
  },
  "& tei-bibl[subtype=misc]": {
    display: "block",
    paddingTop: "25px",
    marginLeft: "25px"
  }
}

const useDisplayStyles = makeStyles(() => ({
  //Items for highlighting semantic markup
  Doubt: {
    "& tei-seg[type=semantic][subtype=doubt]": {
      backgroundColor: Colors.Doubt
    }
  },
  Criticism: {
    "& tei-seg[type=semantic][subtype=criticism]": {
      backgroundColor: Colors.Criticism,
      fontSize: "130"
    }
  },
  Correction: {
    "& tei-seg[type=semantic][subtype=correction]": {
      backgroundColor: Colors.Correction,
      fontSize: "130"
    }
  },
  Praise: {
    "& tei-seg[type=semantic][subtype=praise]": {
      backgroundColor: Colors.Praise,
      fontSize: "130"
    }
  },
  SelfPromotion: {
    "& tei-seg[type=semantic][subtype=self_promotion]": {
      backgroundColor: Colors.SelfPromotion,
      fontSize: "130"
    }
  },
  EyeWitness: {
    "& tei-seg[type=semantic][subtype=witness]": {
      backgroundColor: Colors.EyeWitness,
      fontSize: "130"
    }
  },
  PublicationStatus: {
    "& tei-seg[type=semantic][subtype=pub_status]": {
      backgroundColor: Colors.PublicationStatus,
      fontSize: "130"
    }
  },
  //Items for highlighting bibliographic fields
  Author: {
  // Occidental, Título XXVI. Colectores de libros de Indias, which lists people, not books, makes it necessary to include <person> here alongside <bibl>
  "& tei-bibl[type=orig] > tei-seg tei-author, & tei-bibl[type=orig] > tei-seg tei-editor, & tei-person > tei-seg tei-author, & tei-person > tei-seg tei-editor": {
      borderBottom: `4px solid ${Colors.Author}`,
      fontSize: "130"
    }
  },
  Title: {
    "& tei-bibl[type=orig] > tei-seg tei-title, & tei-person > tei-seg tei-title": {
      borderBottom: `4px solid ${Colors.Title}`,
      fontSize: "130"
    }
  },
  PlaceOfPublication: {
    "& tei-bibl[type=orig] > tei-seg tei-pubPlace, & tei-person > tei-seg tei-pubPlace": {
      borderBottom: `4px solid ${Colors.PlaceOfPublication}`,
      fontSize: "130"
    }
  },
  Printer: {
    "& tei-bibl[type=orig] > tei-seg tei-publisher, & tei-person > tei-seg tei-publisher": {
      borderBottom: `4px solid ${Colors.Printer}`,
      fontSize: "130"
    }
  },
  Date: {
    "& tei-bibl[type=orig] > tei-seg tei-date, & tei-person > tei-seg tei-date": {
      borderBottom: `4px solid ${Colors.Date}`,
      fontSize: "130"
    }
  },
  Size: {
    "& tei-bibl[type=orig] > tei-seg tei-dim, & tei-person > tei-seg tei-dim": {
      borderBottom: `4px solid ${Colors.Size}`,
      fontSize: "130"
    }
  },
  Language: {
    "& tei-bibl[type=orig] > tei-seg tei-textLang, & tei-person > tei-seg tei-textLang": {
      borderBottom: `4px solid ${Colors.Language}`,
      fontSize: "130"
    }
  },
  State: {
    "& tei-bibl[type=orig] > tei-seg tei-seg[subtype=state], & tei-person > tei-seg tei-seg[subtype=state]": {
        borderBottom: `4px solid ${Colors.State}`,
        fontSize: "130"
        }
      },

  // contextual markup
  ContextualDate: {   
    "& tei-date[type=contextual]": {
      fontWeight: "bold"
    }
  },   
  Person: {   
    "& tei-persName[type=contextual]": {
      fontWeight: "bold"
    }
  }, 
  Place: {   
    "& tei-placeName": {
      fontWeight: "bold"
    }
  },

  //Items for displaying/differentiating components of structured annotation
  AuthorsSources: Object.assign({
    "& tei-bibl[subtype=author_source]": {
      display: "block",
      backgroundColor: Colors.AuthorsSources,
      borderStyle: "solid",
      borderWidth: "1px",
      padding: "5px",
      margin: "5px 5px 5px 5px"
    }
  }, hierarchicalStyle),
  RegularizedData: Object.assign({
    "& tei-bibl[subtype=regularized]": {
      display: "block",
      backgroundColor: Colors.RegularizedData,
      borderStyle: "solid",
      borderWidth: "1px",
      padding: "5px",
      margin: "5px 5px 5px 5px"
    }
  }, hierarchicalStyle),
  EditorsSources: Object.assign({
    "& tei-bibl[subtype=editor_source]": {
      display: "block",
      backgroundColor: Colors.EditorsSources,
      borderStyle: "solid",
      borderWidth: "1px",
      padding: "5px",
      margin: "5px 5px 5px 5px"
    }
  }, hierarchicalStyle),
  ModernEditions: Object.assign({
    "& tei-bibl[subtype=modern_version]": {
      display: "block",
      backgroundColor: Colors.ModernEditions,
      borderStyle: "solid",
      borderWidth: "1px",
      padding: "5px",
      margin: "5px 5px 5px 5px"
    }
  }, hierarchicalStyle),
  DigitalVersions: Object.assign({
    "& tei-bibl[subtype=digital_version]": {
      display: "block",
      backgroundColor: Colors.DigitalVersions,
      borderStyle: "solid",
      borderWidth: "1px",
      padding: "5px",
      margin: "5px 5px 5px 5px"
    }
  }, hierarchicalStyle),
  //Items for displaying/differentiating components of unstructured annotation
  LexicalNotes: {
    "& tei-note[type=lexical]": {
      display: "block",
      backgroundColor: Colors.LexicalNotes,
      borderStyle: "solid",
      borderWidth: "1px",
      padding: "5px",
      margin: "5px 5px 5px 5px"
    }
  },
  ContextualNotes: {
    "& tei-note[type=contextual]": {
      display: "block",
      backgroundColor: Colors.ContextualNotes,
      borderStyle: "solid",
      borderWidth: "1px",
      padding: "5px",
      margin: "5px 5px 5px 5px"
    }
  },
  BibliographicalNotes: {
    "& tei-note[type=bibliographical]": {
      display: "block",
      backgroundColor: Colors.BibliographicalNotes,
      borderStyle: "solid",
      borderWidth: "1px",
      padding: "5px",
      margin: "5px 5px 5px 5px"
    }
  },
  //For switching the way original entries are displayed 
  HierarchicalView: hierarchicalStyle,
  PrimaryOnly: {
    "& tei-bibl[subtype=other_edition]": {
      display: "none"
    },
    "& tei-bibl[subtype=translation]": {
      display: "none"
    }
  }
}
))


export default useDisplayStyles